<?php defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Test_model extends Eloquent
{
    protected $table = 'tests';
}

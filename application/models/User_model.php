<?php defined('BASEPATH') or exit('No direct script access allowed');

@include_once APPPATH . 'models/Group_model.php';

use \Illuminate\Database\Eloquent\Model as Eloquent;

class User_model extends Eloquent
{
    protected $table = 'users';

    public $timestamps = false;

    protected $hidden = ['id', 'password'];

    protected $fillable = [
        'ip_address',
        'username',
        'password',
        'salt',
        'email',
        'activation_code',
        'forgotten_password_code',
        'created_on',
        'last_login',
        'active',
        'first_name',
        'last_name',
        'company',
        'phone',
    ];

    public function groups()
    {
        return $this->belongsToMany('Group_model', 'users_groups', 'user_id', 'group_id');
    }

    /**
     * Sync group relation adding new groups as needed
     *
     * @param array $groups
     */
    public function syncGroups(array $groups = [])
    {
        Group_model::addNeededGroups($groups);
        if (count($groups)) {
            $this->groups()->sync(
                Group_model::whereIn('name', $groups)->pluck('id')
            );
            return;
        }
        $this->groups()->detach();
    }
}

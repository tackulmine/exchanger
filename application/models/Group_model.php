<?php defined('BASEPATH') or exit('No direct script access allowed');

@include_once APPPATH . 'models/User_model.php';

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Group_model extends Eloquent
{
    protected $table = 'categories';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
    ];

    protected $casts = [
    ];

    public function users()
    {
        return $this->belongsToMany('User_model', 'users_groups', 'group_id', 'user_id');
    }

    /**
     * Add any groups needed from the list
     *
     * @param array $groups List of groups to check/add
     */
    public static function addNeededGroups(array $groups = [])
    {
        $CI = &get_instance();
        if (count($groups) === 0) {
            return;
        }
        $found = static::whereIn('name', $groups)->pluck('name')->toArray();
        foreach (array_diff($groups, $found) as $group) {
            static::create([
                'name' => $group,
            ]);
        }
    }
}

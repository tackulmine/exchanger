<?php defined('BASEPATH') OR exit('No direct script access allowed');

@include_once(APPPATH . 'models/Post_model.php');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Tag_model extends Eloquent
{
    protected $table = 'tags';

    protected $fillable = [
        'slug',
        'title',
        'subtitle',
        'description',
        'image_path',
        'image',
        'meta_title',
        'meta_description',
        'is_draft',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'is_draft' => 'boolean',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function posts()
    {
        return $this->belongsToMany('Post_model', 'post_tag_pivot', 'tag_id', 'post_id');
    }

    /**
     * Return URL to tag
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        $url = site_url('topik/' . $this->slug);
        return $url;
    }

    /**
     * Add any tags needed from the list
     *
     * @param array $tags List of tags to check/add
     */
    public static function addNeededTags(array $tags = [])
    {
        $CI =& get_instance();
        $CI->load->library('ion_auth');
        if (count($tags) === 0) {
            return;
        }
        $found = static::whereIn('title', $tags)->pluck('title')->toArray();
        foreach (array_diff($tags, $found) as $tag) {
            static::create([
                'title' => $tag,
                'slug' => url_title($tag, '-', true),
                'subtitle' => 'Subtitle for '.$tag,
                'is_draft' => 1,
                'meta_description' => '',
                'created_by' => $CI->ion_auth->user()->row()->id,
            ]);
        }
    }

    public function scopePublished($query)
    {
        return $query->where('is_draft', '0');
    }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

@include_once APPPATH . 'models/Post_model.php';

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Category_model extends Eloquent
{
    protected $table = 'categories';

    protected $fillable = [
        'slug',
        'title',
        'subtitle',
        'description',
        'image_path',
        'image',
        'meta_title',
        'meta_description',
        'is_draft',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'is_draft' => 'boolean',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function posts()
    {
        return $this->hasMany('Post_model', 'category_id');
    }

    /**
     * Return URL to category
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        $url = site_url('kategori/' . $this->slug);
        return $url;
    }

    public function scopePublished($query)
    {
        return $query->where('is_draft', '0');
    }
}

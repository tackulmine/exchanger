<?php defined('BASEPATH') or exit('No direct script access allowed');

@include_once APPPATH . 'models/Category_model.php';
@include_once APPPATH . 'models/Tag_model.php';

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Post_model extends Eloquent
{
    protected $table = 'posts';

    protected $dates = ['published_at'];

    protected $fillable = [
        'category_id',
        'slug',
        'title',
        'subtitle',
        'content_raw',
        'content_html',
        'image_path',
        'image',
        'meta_title',
        'meta_description',
        'is_draft',
        'published_at',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'is_draft' => 'boolean',
        'created_by' => 'integer',
        'updated_by' => 'integer',
    ];

    public function category()
    {
        return $this->belongsTo('Category_model');
    }

    public function tags()
    {
        return $this->belongsToMany('Tag_model', 'post_tag_pivot', 'post_id', 'tag_id');
    }

    /**
     * Return URL to post
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        $url = site_url('artikel/' . $this->slug);
        return $url;
    }

    /**
     * Sync tag relation adding new tags as needed
     *
     * @param array $tags
     */
    public function syncTags(array $tags = [])
    {
        Tag_model::addNeededTags($tags);
        if (count($tags)) {
            $this->tags()->sync(
                Tag_model::whereIn('title', $tags)->pluck('id')
            );
            return;
        }
        $this->tags()->detach();
    }

    /**
     * Return array of tag links
     *
     * @param string $base
     * @return array
     */
    public function tagLinks($base = '/blog?tag=%TAG%')
    {
        $tags = $this->tags()->pluck('title');
        $return = [];
        foreach ($tags as $tag) {
            $url = str_replace('%TAG%', urlencode($tag), $base);
            $return[] = '<a href="' . $url . '">' . e($tag) . '</a>';
        }
        return $return;
    }

    /**
     * Return next post after this one or null
     *
     * @param Tag $tag
     * @return Post
     */
    public function newerPost(Tag_model $tag = null)
    {
        $query = static::where('published_at', '>', $this->published_at)
            ->published()
            ->orderBy('published_at', 'asc');
        if ($tag) {
            $query = $query->whereHas('tags', function ($q) use ($tag) {
                $q->where('tag', '=', $tag->tag);
            });
        }
        return $query->first();
    }

    /**
     * Return older post before this one or null
     *
     * @param Tag $tag
     * @return Post
     */
    public function olderPost(Tag_model $tag = null)
    {
        $query = static::where('published_at', '<', $this->published_at)
            ->published()
            ->orderBy('published_at', 'desc');
        if ($tag) {
            $query = $query->whereHas('tags', function ($q) use ($tag) {
                $q->where('tag', '=', $tag->tag);
            });
        }
        return $query->first();
    }

    public function scopePublished($query)
    {
        return $query->where('is_draft', '0')
            ->whereNotNull('published_at')
            ->where('published_at', '<=', Carbon\Carbon::now());
    }

    public function scopeHasCover($query)
    {
        return $query->whereNotNull('image');
    }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('btcid_query')) {
    function btcid_query($url = '', $method = "get", array $req = [])
    {
        // API settings
        $key = INDODAX_API_KEY; // your API-key
        $secret = INDODAX_API_SECRET; // your Secret-key
        $req['method'] = $method;
        $req['nonce'] = time();
        // generate the POST data string
        $post_data = http_build_query($req, '', '&');
        $sign = hash_hmac('sha512', $post_data, $secret);
        // generate the extra headers
        $headers = [
            'Sign: ' . $sign,
            'Key: ' . $key,
        ];
        // our curl handle (initialize if required)
        static $ch = null;
        if (is_null($ch)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; INDODAXCOM PHP client; ' . php_uname('s') . '; PHP/' . phpversion() . ')');
        }
        curl_setopt($ch, CURLOPT_URL, 'https://indodax.com/api/' . $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // for php version 7.2
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // run the query
        $res = curl_exec($ch);
        if ($res === false) {
            throw new Exception('Could not get reply:' . curl_error($ch));
        }

        // $dec = json_decode($res, true);
        // if (!$dec) {
        //     throw new Exception('Invalid data received, please make sure connection is working and requested API exists: ' . $res);
        // }

        curl_close($ch);
        $ch = null;
        return $res;
    }
}
// $result = btcid_query('tickers');
// print_r($result);

if (!function_exists('convert_indodax_ticker')) {
    function convert_indodax_ticker($type = "btc_idr", $price = "")
    {
        $tmp = [];
        switch ($type) {
            // case 'btc_idr':
            //     $tmp = [
            //         'sell' => $price - ($price * 0.01),
            //         'buy' => $price + ($price * 0.01),
            //     ];
            //     break;
            // case 'bchsv_idr':
            //     $tmp = [
            //         'sell' => $price - ($price * 0.01),
            //         'buy' => $price + ($price * 0.01),
            //     ];
            //     break;
            // case 'bchabc_idr':
            //     $tmp = [
            //         'sell' => $price - ($price * 0.01),
            //         'buy' => $price + ($price * 0.01),
            //     ];
            //     break;
            // case 'eth_idr':
            //     $tmp = [
            //         'sell' => $price - ($price * 0.01),
            //         'buy' => $price + ($price * 0.01),
            //     ];
            //     break;
            case 'ltc_idr':
                $tmp = [
                    'sell' => $price - ($price * 0.02),
                    'buy' => $price + ($price * 0.01),
                ];
                break;
            case 'etc_idr':
                $tmp = [
                    'sell' => $price - ($price * 0.03),
                    'buy' => $price + ($price * 0.01),
                ];
                break;
            case 'xrp_idr':
                $tmp = [
                    'sell' => $price - ($price * 0.04),
                    'buy' => $price + ($price * 0.01),
                ];
                break;
            case 'str_idr':
                $tmp = [
                    'sell' => $price - ($price * 0.04),
                    'buy' => $price + ($price * 0.03),
                ];
                break;
            case 'usdt_idr':
                $tmp = [
                    'sell' => $price - ($price * 0.03),
                    'buy' => $price + ($price * 0.06),
                ];
                break;

            default:
                $tmp = [
                    'sell' => $price - ($price * 0.01),
                    'buy' => $price + ($price * 0.01),
                ];
                break;
        }
        return $tmp;
    }
}

if (!function_exists('convert_indodax_tickers')) {
    function convert_indodax_tickers($tickers, $filter_tickers = [])
    {
        if (count($filter_tickers) > 0) {
            // $filter_tickers = array_flip($filter_tickers);
            $tickers = array_intersect_key($tickers, $filter_tickers);
        }
        $tmp_tickers = [];
        foreach ($tickers as $key => $values) {
            $price = $values['last'];
            switch ($key) {
                case 'str_idr':
                    $zaky_addt_values = [
                        'zaky_sell' => $price - ($price * 0.04),
                        'zaky_buy' => $price + ($price * 0.03),
                    ];
                    break;
                case 'usdt_idr':
                    $zaky_addt_values = [
                        'zaky_sell' => $price - ($price * 0.03),
                        'zaky_buy' => $price + ($price * 0.06),
                    ];
                    break;

                case 'etc_idr':
                    $zaky_addt_values = [
                        'zaky_sell' => $price - ($price * 0.03),
                        'zaky_buy' => $price + ($price * 0.01),
                    ];
                    break;

                case 'xrp_idr':
                    $zaky_addt_values = [
                        'zaky_sell' => $price - ($price * 0.04),
                        'zaky_buy' => $price + ($price * 0.01),
                    ];
                    break;

                case 'ltc_idr':
                    $zaky_addt_values = [
                        'zaky_sell' => $price - ($price * 0.02),
                        'zaky_buy' => $price + ($price * 0.01),
                    ];
                    break;

                default:
                    $zaky_addt_values = [
                        'zaky_sell' => $price - ($price * 0.01),
                        'zaky_buy' => $price + ($price * 0.01),
                    ];
                    break;
            }
            $symbol_key = str_replace("_idr", "", $key);
            switch ($symbol_key) {
                case 'str':
                    $zaky_addt_values = [
                        'symbol' => 'xlm',
                        'name' => $filter_tickers[$key],
                    ] + $zaky_addt_values;
                    break;
                case 'bchabc':
                    $zaky_addt_values = [
                        'symbol' => 'bch',
                        'name' => $filter_tickers[$key],
                    ] + $zaky_addt_values;
                    break;
                case 'bchsv':
                    $zaky_addt_values = [
                        'symbol' => 'bsv',
                        'name' => $filter_tickers[$key],
                    ] + $zaky_addt_values;
                    break;

                default:
                    $zaky_addt_values = [
                        'symbol' => $symbol_key,
                        'name' => $filter_tickers[$key],
                    ] + $zaky_addt_values;
                    break;
            }
            $tmp_tickers[$key] = $values + $zaky_addt_values;
        }
        return $tmp_tickers;
    }
}

if (!function_exists('convert_coingecko_markets')) {
    function convert_coingecko_markets($markets, $filter_markets = [])
    {
        $CI = &get_instance();
        $CI->load->helper('short_number_helper');

        if (count($filter_markets) > 0) {
            $markets = array_intersect_key($markets, array_flip($filter_markets));
        }
        // var_dump($markets); die;
        $tmp_markets = [];
        foreach ($markets as $key => $values) {
            $zaky_addt_values = [
                'market_cap_short' => number_format_short($values['market_cap']),
                'total_volume_short' => number_format_short($values['total_volume']),
            ];
            $tmp_markets[$key] = $values + $zaky_addt_values;
            // var_dump($tmp_markets[$key]); die;
        }
        return $tmp_markets;
    }
}

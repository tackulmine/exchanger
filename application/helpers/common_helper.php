<?php defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('rupiah')) {
    function rupiah($angka)
    {
        $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
        return $hasil_rupiah;
    }
}

if (!function_exists('percent')) {
    function percent($angka)
    {
        $percent = number_format($angka, 2, ',', '.');
        return $percent;
    }
}

if (!function_exists('setKeyBy')) {
    function setKeyBy($keyBy="", $data)
    {
        $tmp = [];
        foreach ($data as $values) {
            $values = (array) $values;
            $tmp[$values[$keyBy]] = $values;
        }
        return $tmp;
    }
}

if (!function_exists('resizeImage')) {
    /**
     * Manage uploadImage
     *
     * @return Response
     */
    function resizeImage($filename, $dir = 'uploads', $width = 150, $height = 150)
    {
        $CI = &get_instance();
        $CI->load->library('image_lib');

        $dir = rtrim($dir, '/');
        $source_image = $dir . DIRECTORY_SEPARATOR . $filename;
        $target_path = $dir;

        if (!is_dir(FCPATH . $target_path)) {
            mkdir(FCPATH . $target_path, 0777, true);
        }

        $new_image = $target_path . $filename;

        // resize image
        $config_manip = [
            'image_library' => 'gd2',
            'source_image' => './' . $source_image,
            'new_image' => './' . $new_image,
            'maintain_ratio' => true,
            'create_thumb' => true,
            'thumb_marker' => '_150x0',
            'width' => $width,
            'height' => $height,
        ];

        $CI->image_lib->initialize($config_manip);

        if (!$CI->image_lib->resize()) {
            echo $CI->image_lib->display_errors();
        }

        $CI->image_lib->clear();
    }
}


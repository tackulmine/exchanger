
  <?php /* ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <?php //*/ ?>

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Developed by meftahul.com
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#"><?php echo APP_NAME; ?></a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="/assets/vendor/admin-lte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/assets/vendor/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/assets/vendor/admin-lte/plugins/select2/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="/assets/vendor/admin-lte/plugins/moment/moment.min.js"></script>
<?php /* ?>
<script src="/assets/vendor/admin-lte/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<?php //*/ ?>
<!-- date-range-picker -->
<script src="/assets/vendor/admin-lte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- AdminLTE App -->
<script src="/assets/vendor/admin-lte/dist/js/adminlte.min.js"></script>
<?php /* ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>
<?php //*/ ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/codemirror.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/mode/xml/xml.js"></script>
<script src="/assets/summernote/summernote-bs4.js"></script>
<script src="/assets/summernote/lang/summernote-id-ID.js"></script>
<script src="/assets/vendor/admin-lte/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<?php /* ?>
<script>
var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
</script>
<?php //*/ ?>
<script>
$(document).ready(function() {

  //Initialize maxlength
  $('[maxlength]').maxlength({
    alwaysShow: true,
    warningClass: "badge badge-success",
    limitReachedClass: "badge badge-danger",
  });

  //Initialize Select2 Elements
  $("select:not('.without-select2')").select2({
    tags: true,
    createTag: function(params) {
      return {
        id: params.term,
        text: params.term,
        newOption: true
      };
    },
    templateResult: function(data) {
      var result = data.text;
      if (data.newOption) {
        result = result + ' (new)';
      }
      return result;
    },
    theme: 'bootstrap4'
  });

  $('.editor').summernote({
    height: "300px",
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      // ['fontname', ['fontname']],
      // ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video']],
      ['view', ['fullscreen', 'codeview', 'help']],
    ],
    codemirror: { // codemirror options
      theme: 'monokai',
      lineNumbers: true,
      lineWrapping: true
    },
    lang: 'id-ID',
    stripTags: true,
    styleTags: [
      'p',
      'blockquote',
      'code',
      'h3',
      'h4',
      'h5',
      'h6',
    ],
    callbacks: {
      onImageUpload: function(image) {
        uploadImage($(this), image[0]);
      },
      onMediaDelete: function(target) {
        deleteImage(target[0].src);
      },
      // onPaste: function (e) {
      //     var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
      //     e.preventDefault();
      //     document.execCommand('insertText', false, bufferText);
      // }
      onPaste: function(e) {
        var thisNote = $(this);
        var updatePastedText = function(someNote) {
          var original = someNote.summernote('code');
          var microsoft = false;
          if (original.indexOf("mso-") >= 0) microsoft = true;
          var cleaned = CleanPastedHTML(original, microsoft); //this is where to call whatever clean function you want. I have mine in a different file, called CleanPastedHTML.
          someNote.summernote('code', cleaned); //this sets the displayed content editor to the cleaned pasted code.
          if (microsoft) alert("Pasted content contained Microsoft Word formatting and has been removed.");
        };
        setTimeout(function() { updatePastedText(thisNote); }, 10);
      },
    }
  });

  function CleanPastedHTML(input, is_ms) {
    // 1. remove line breaks / Mso classes
    var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g;
    var output = input.replace(stringStripper, ' ');
    // 2. strip Word generated HTML comments
    var commentSripper = new RegExp('<!--(.*?)-->', 'g');
    output = output.replace(commentSripper, '');
    var tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>', 'gi');
    // 3. remove tags leave content if any
    output = output.replace(tagStripper, '');
    // 4. Remove everything in between and including tags '<style(.)style(.)>'
    var badTags = ['style', 'script', 'applet', 'embed', 'noframes', 'noscript'];

    for (var i = 0; i < badTags.length; i++) {
      tagStripper = new RegExp('<' + badTags[i] + '.*?' + badTags[i] + '(.*?)>', 'gi');
      output = output.replace(tagStripper, '');
    }
    // 5. remove attributes ' style="..."'
    var badAttributes = ['style', 'start'];
    if (is_ms) badAttributes = ['style', 'start'];
    for (var i = 0; i < badAttributes.length; i++) {
      var attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"', 'gi');
      output = output.replace(attributeStripper, '');
    }

    return output;
  }

  function uploadImage($sel, image) {
    var data = new FormData();
    data.append("image", image);
    // data.append(csrfName, csrfHash);
    $.ajax({
      url: "<?php echo site_url($admin_base_url . 'editor/upload_image')?>",
      cache: false,
      contentType: false,
      processData: false,
      data: data,
      type: "POST",
      success: function(url) {
        $sel.summernote("insertImage", url);
      },
      error: function(data) {
        console.log(data);
      }
    });
  }

  function deleteImage(src) {
    $.ajax({
      data: { src: src },
      type: "POST",
      url: "<?php echo site_url($admin_base_url . 'editor/delete_image')?>",
      cache: false,
      success: function(response) {
        console.log(response);
      }
    });
  }

  function readURL(input, target) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $(target).attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }

  $(".upload_image_preview").change(function(e) {
    readURL(this, $(this).data('target'));
  });

  //Date range picker with time picker
  $('.single-datetimepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    // minDate: moment(),
    timePicker: true,
    timePicker24Hour: true,
    // timePickerIncrement: 5,
    // timePickerSeconds: true,
    drops: "up",
    locale: {
      format: 'DD/MM/YYYY HH:mm',"daysOfWeek": [
            "Mg",
            "Sn",
            "Sl",
            "Rb",
            "Km",
            "Jm",
            "Sb"
        ],
        "monthNames": [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "Nopember",
            "Desember"
        ],
    }
  });

});
</script>

</body>
</html>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php echo empty($page_title) ? "Dashboard" : $page_title; ?> | <?php echo APP_NAME; ?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/assets/vendor/admin-lte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="/assets/vendor/admin-lte/plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="/assets/vendor/admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/vendor/admin-lte/dist/css/adminlte.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="/assets/vendor/admin-lte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/assets/vendor/admin-lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <?php /* ?>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
  <?php //*/ ?>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/codemirror.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.48.4/theme/monokai.min.css">
  <link rel="stylesheet" href="/assets/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <?php /* ?>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo site_url('deminz/'); ?>" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo site_url('deminz/auth/logout'); ?>" class="nav-link">Logout</a>
      </li>
      <?php //*/ ?>
    </ul>

    <?php /* ?>
    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    <?php //*/ ?>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <?php /* ?>
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fas fa-th-large"></i></a>
      </li>
      <?php //*/ ?>
      <li class="nav-item">
        <a class="nav-link" onclick="return confirm('Sure to logout?');" href="<?php echo site_url('deminz/auth/logout'); ?>"><i class="fas fa-power-off"></i> Logout</a>
      </li>
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo site_url($admin_base_url); ?>" class="brand-link">
      <!-- <img src="/assets/vendor/admin-lte/dist/img/AdminLTELogo.png" alt="<?php echo APP_NAME; ?>" class="brand-image img-circle elevation-3" -->
      <img src="/assets/img/zakycell-small-logo.png" alt="<?php echo APP_NAME; ?>" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><?php echo APP_NAME; ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!-- <img src="/assets/vendor/admin-lte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
          <img src="<?php echo gravatar($current_user->email, 160, 'g', true); ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#!" class="d-block"><?php echo $current_user->first_name; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo site_url($admin_base_url . 'dashboard'); ?>"
              class="nav-link <?php echo ($this->uri->segment(2) == 'dashboard' or empty($this->uri->segment(2))) ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview <?php echo stristr($this->uri->uri_string(), 'categories') ? 'menu-open' : '' ?>">
            <a href="#!"
              class="nav-link <?php echo stristr($this->uri->uri_string(), 'categories') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-folder"></i>
              <p>
                Categories
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url($admin_base_url . 'categories'); ?>"
                  class="nav-link <?php echo ($this->uri->segment(2) == 'categories' and (empty($this->uri->segment(3)) or $this->uri->segment(3) == 'page' or $this->uri->segment(3) == 'index')) ? 'active' : '' ?>">
                  <i class="fa fa-table nav-icon"></i>
                  <p>Manage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url($admin_base_url . 'categories/create'); ?>"
                  class="nav-link <?php echo stristr($this->uri->uri_string(), 'categories/create') ? 'active' : '' ?>">
                  <i class="fa fa-pencil-alt nav-icon"></i>
                  <p>Create</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php echo stristr($this->uri->uri_string(), 'tags') ? 'menu-open' : '' ?>">
            <a href="#!"
              class="nav-link <?php echo stristr($this->uri->uri_string(), 'tags') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tag"></i>
              <p>
                Tags
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url($admin_base_url . 'tags'); ?>"
                  class="nav-link <?php echo ($this->uri->segment(2) == 'tags' and (empty($this->uri->segment(3)) or $this->uri->segment(3) == 'page' or $this->uri->segment(3) == 'index')) ? 'active' : '' ?>">
                  <i class="fa fa-table nav-icon"></i>
                  <p>Manage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url($admin_base_url . 'tags/create'); ?>"
                  class="nav-link <?php echo stristr($this->uri->uri_string(), 'tags/create') ? 'active' : '' ?>">
                  <i class="fa fa-pencil-alt nav-icon"></i>
                  <p>Create</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php echo stristr($this->uri->uri_string(), 'posts') ? 'menu-open' : '' ?>">
            <a href="#!"
              class="nav-link <?php echo stristr($this->uri->uri_string(), 'posts') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Posts
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url($admin_base_url . 'posts'); ?>"
                  class="nav-link <?php echo ($this->uri->segment(2) == 'posts' and (empty($this->uri->segment(3)) or $this->uri->segment(3) == 'page' or $this->uri->segment(3) == 'index')) ? 'active' : '' ?>">
                  <i class="fa fa-table nav-icon"></i>
                  <p>Manage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url($admin_base_url . 'posts/create'); ?>"
                  class="nav-link <?php echo stristr($this->uri->uri_string(), 'posts/create') ? 'active' : '' ?>">
                  <i class="fa fa-pencil-alt nav-icon"></i>
                  <p>Create</p>
                </a>
              </li>
              <?php /* ?>
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Active Page</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inactive Page</p>
                </a>
              </li>
              <?php //*/ ?>
            </ul>
          </li>
          <?php /* ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Simple Link
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
          <?php //*/ ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--
        Tinker CSS Template
        https://templatemo.com/tm-506-tinker
        -->
        <title><?php echo ($meta_title ?? APP_NAME . ' - Jual/Beli Crypto, Jasa WD Indodax'); ?></title>
        <meta name="description" content="<?php echo ($meta_description ?? 'Zaky CELL - Jual/Beli Crypto BTC, ETH dan Altcoin, Jasa Penarikan Indodax, Deposit Indodax, Voucher Indodax, Layanan Deposit PayPal, Layanan Deposit PerfectMoney, Zaky Cell Adalah Mitra Resmi Indodax.'); ?>">
        <meta name="author" content="Zaky CELL - Jual/Beli Crypto, Voucher Indodax">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/bootstrap-theme.min.css">
        <?php /* ?>
        <link rel="stylesheet" href="/assets/css/fontAwesome.css">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
        <?php //*/ ?>
        <link rel="stylesheet" href="/assets/vendor/admin-lte/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="/assets/css/hero-slider.css">
        <link rel="stylesheet" href="/assets/css/owl-carousel.css">
        <link rel="stylesheet" href="/assets/css/templatemo-style.css?<?php echo filemtime(FCPATH . '/assets/css/templatemo-style.css'); ?>">
        <link rel="stylesheet" href="/assets/css/lightbox.css">

        <script src="/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

<body>
    <div class="header <?php echo ($this->uri->segment(1) != "") ? 'always-active active' : ''; ?>">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo site_url(); ?>" class="navbar-brand"><img src="/assets/img/zakycell-small-logo.png" width="50px" height="50px" style="margin-top: 15px;" />
                </div>
                <!--/.navbar-header-->
                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <?php if (!$this->uri->segment(1) or $this->uri->segment(1) == 'home'): ?>
                            <li><a href="#" class="scroll-top">Home</a></li>
                            <li><a href="#" class="scroll-link" data-id="about">Tentang</a></li>
                            <?php /* ?>
                            <li><a href="#" class="scroll-link" data-id="portfolio">Portfolio</a></li>
                            <?php //*/ ?>
                            <li><a href="#" class="scroll-link" data-id="service">Layanan</a></li>
                            <?php /* ?>
                            <li><a href="#" class="scroll-link" data-id="testimonial">Testimonial</a></li>
                            <?php //*/ ?>
                            <li><a href="#" class="scroll-link" data-id="price">Crypto</a></li>
                            <?php /* ?>
                            <li><a href="#" class="scroll-link" data-id="blog">Blog</a></li>
                            <?php //*/ ?>
                            <li><a href="<?php echo site_url('blog') ?>">Blog</a></li>
                            <li><a href="#" class="scroll-link" data-id="contact-us">Kontak</a></li>
                        <?php else: ?>
                            <li><a href="/#">Home</a></li>
                            <li><a href="/#about">Tentang</a></li>
                            <li><a href="/#service">Layanan</a></li>
                            <li><a href="/#price">Crypto</a></li>
                            <li><a href="<?php echo site_url('blog') ?>">Blog</a></li>
                            <li><a href="/#contact-us">Kontak</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-->

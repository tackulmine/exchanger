    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="logo">
                        <a class="logo-ft scroll-top" href="#">
                          <img src="/assets/img/zakycell-small-logo.png" width="26px" style="margin-right: 10px"> <em>Z</em>akyCell
                        </a>
                        <p>Copyright &copy; 2020 Zaky Cell
                       <br>Design: TemplateMo</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="location">
                        <h4>Location</h4>
                        <ul>
                            <li>Dusun Parsehan, <br>Desa/Kel. Tamansari</li>
                            <li>Kec. Dringu, Probolinggo, <br>Jawa Timur 67271, Indonesia</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="contact-info">
                        <h4>More Info</h4>
                        <ul>
                            <li><em>Phone</em>: <a href="tel:+6282330718333">+62823-3071-8333</a></li>
                            <li><em>Bitcointalk</em>: <a href="https://bitcointalk.org/index.php?action=profile;u=839568">AakZaki</a></li>
                            <li><em>Verifikasi Address</em>: <a href="https://btc.com/1ZAKiBoZ6uq3rRBuzhCZMNVfoCTau4SPS">1ZAKiBoZ6uq3rRBuzhCZMNVfoCTau4SPS</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="connect-us">
                        <h4>Get Social with us</h4>
                        <ul>
                            <li><a href="https://t.me/AakZaki" target="_blank"><i class="fab fa-telegram"></i></a></li>
                            <li><a href="https://wa.me/6282330718333" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                            <li><a href="https://www.facebook.com/AakZaki.PartnerIndodax" target="_blank"><i class="fab fa-facebook"></i></a></li>
                            <li><a href="mailto:cs@zaky-cell.com" target="_blank"><i class="fas fa-envelope"></i></a></li>
                            <li><a href="https://btc.com/1ZAKiBoZ6uq3rRBuzhCZMNVfoCTau4SPS" target="_blank"><i class="fab fa-bitcoin"></i></a></li>
                            <?php /* ?>
                            <li><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-dribbble"></i></a></li>
                            <?php //*/ ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="/assets/js/vendor/jquery-1.11.2.min.js"></script>
    <script>window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"><\/script>')</script>

    <script src="/assets/js/vendor/bootstrap.min.js"></script>

    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/main.js?<?php echo filemtime(FCPATH . '/assets/js/main.js'); ?>"></script>

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" type="text/javascript"></script>
    <script src="/assets/vendor/matchHeight/dist/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // navigation click actions
            $('.scroll-link').on('click', function(event){
                event.preventDefault();
                var sectionID = $(this).attr("data-id");
                scrollToID('#' + sectionID, 750);
            });
            // scroll to top action
            $('.scroll-top').on('click', function(event) {
                event.preventDefault();
                $('html, body').animate({scrollTop:0}, 'slow');
            });
            // mobile nav toggle
            $('#nav-toggle').on('click', function (event) {
                event.preventDefault();
                $('#main-nav').toggleClass("open");
            });
            $('.post-details').matchHeight();
        });
        // scroll function
        function scrollToID(id, speed){
            var offSet = 50;
            var targetOffset = $(id).offset().top - offSet;
            var mainNav = $('#main-nav');
            $('html,body').animate({scrollTop:targetOffset}, speed);
            if (mainNav.hasClass("open")) {
                mainNav.css("height", "1px").removeClass("in").addClass("collapse");
                mainNav.removeClass("open");
            }
        }
        if (typeof console === "undefined") {
            console = {
                log: function() { }
            };
        }
    </script>

    <script src="https://www.google.com/recaptcha/api.js?render=<?php echo RECAPTCHA_SITE_KEY; ?>"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('<?php echo RECAPTCHA_SITE_KEY; ?>', {action: 'order'}).then(function(token) {
           if (token && document.getElementById('order_recaptcha')) {
                document.getElementById('order_recaptcha').value = token;
            }
        });
        grecaptcha.execute('<?php echo RECAPTCHA_SITE_KEY; ?>', {action: 'contact'}).then(function(token) {
           if (token && document.getElementById('contact_recaptcha')) {
                document.getElementById('contact_recaptcha').value = token;
            }
        });
    });
    </script>
</body>
</html>

<h4>S E O</h4>
<div class="form-group row">
  <label for="meta_title" class="col-sm-2 col-form-label">Meta Title</label>
  <div class="col-sm-10">
    <input type="text" name="meta_title" id="meta_title" maxlength="60" value="<?php echo set_value('meta_title', $meta_title); ?>" class="form-control" placeholder="">
  </div>
</div>
<div class="form-group row">
  <label for="meta_description" class="col-sm-2 col-form-label">Meta Description</label>
  <div class="col-sm-10">
    <textarea name="meta_description" id="meta_description" cols="30" rows="3" maxlength="160" class="form-control" placeholder=""><?php echo set_value('meta_description', $meta_description); ?></textarea>
  </div>
</div>
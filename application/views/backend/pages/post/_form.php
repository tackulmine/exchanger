<div class="form-group row">
  <label for="title" class="col-sm-2 col-form-label">Title</label>
  <div class="col-sm-10">
    <input type="text" name="title" id="title" maxlength="100" value="<?php echo set_value('title', $title); ?>" class="form-control" placeholder="Title" required autofocus>
  </div>
</div>
<div class="form-group row">
  <label for="category_id" class="col-sm-2 col-form-label">Category</label>
  <div class="col-sm-10">
    <?php echo form_dropdown('category_id', $category_options, set_value('category_id', $category_id), 'class="form-control" id="category_id" required'); ?>
  </div>
</div>
<div class="form-group row">
  <label for="subtitle" class="col-sm-2 col-form-label">Subtitle</label>
  <div class="col-sm-10">
    <textarea name="subtitle" id="subtitle" cols="30" rows="3" class="form-control" placeholder="Subtitle"><?php echo set_value('subtitle', $subtitle); ?></textarea>
  </div>
</div>
<div class="form-group row">
  <label for="image" class="col-sm-2 col-form-label">Photo Cover</label>
  <div class="col-sm-10">
    <img id="preview_image" src="<?php echo (!empty($image)) ? "/{$image_path}{$image}" : "#" ?>" alt="" class="mb-1 img-fluid" />
    <input type="file" name="image" id="image" class="upload_image_preview" data-target="#preview_image">
  </div>
</div>
<div class="form-group row">
  <label for="content_html" class="col-sm-2 col-form-label">Description</label>
  <div class="col-sm-10">
    <textarea name="content_html" id="content_html" cols="30" rows="10" class="form-control editor" placeholder="Description" required><?php echo set_value('content_html', $content_html); ?></textarea>
  </div>
</div>
<div class="form-group row">
  <label for="tags" class="col-sm-2 col-form-label">Tags</label>
  <div class="col-sm-10">
    <?php echo form_dropdown('tags[]', $tag_options, set_value('tags', $tags), 'class="form-control" id="tags" multiple'); ?>
  </div>
</div>
<div class="form-group row">
  <div class="offset-sm-2 col-sm-10">
    <?php /* ?>
    <div class="form-check">
      <input type="checkbox" class="form-check-input" name="is_draft" id="is_draft" value="1" <?php echo set_checkbox('is_draft', '1', (isset($is_draft) ? (bool) $is_draft : true)); ?>>
      <label class="form-check-label" for="is_draft"> is draft?</label>
    </div>
    <?php //*/ ?>
    <div class="icheck-default d-inline">
      <input type="checkbox" name="is_draft" id="is_draft" value="1" <?php echo set_checkbox('is_draft', '1', (isset($is_draft) ? (bool) $is_draft : true)); ?>>
      <label for="is_draft"> is draft?</label>
    </div>
  </div>
</div>
<div class="form-group row">
  <label for="published_at" class="col-sm-2 col-form-label">Published at</label>
  <div class="col-sm-10">
    <?php echo form_input('published_at', set_value('published_at', (!empty($published_at) ? date("d/m/Y H:i", strtotime($published_at)) : '') ), 'class="form-control single-datetimepicker"'); ?>
  </div>
</div>
<?php $this->load->view('backend/pages/partials/seo_form'); ?>
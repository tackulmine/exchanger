  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $module_name; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url($admin_base_url) ?>">Dashboard</a></li>
              <li class="breadcrumb-item active"><?php echo $module_name; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title mt-0 mt-md-2 mb-2"><?php echo $page_title; ?></h3>
                <div class="card-tools">
                  <?php echo form_open($admin_base_url . 'posts', ['method' => 'get']); ?>
                  <div class="row no-gutters">
                    <div class="col-md-3 col-12">
                      <div class="input-group mt-0 mb-1 mb-md-0">
                        <?php echo form_input('keyword', $this->input->get('keyword'), 'class="form-control float-right" placeholder="Search.."'); ?>
                      </div>
                    </div>
                    <div class="col-md-9 col-12">
                      <div class="input-group my-0"
                        <?php /* ?>
                        style="width: 400px;"
                        <?php //*/ ?>
                        >
                        <?php /* ?>
                        <input type="text" name="keyword" class="form-control float-right" placeholder="Search">
                        <select name="filter_draft" class="form-control float-right" placeholder="Is Draft?">
                          option
                        </select>
                        <?php //*/ ?>
                        <?php echo form_dropdown('filter_draft', [''=>'draft?', 'n'=>'No', 'y'=>'Yes'], $this->input->get('filter_draft'), 'class="form-control float-right"'); ?>
                        <?php echo form_dropdown('filter_category', $category_options, $this->input->get('filter_category'), 'class="form-control float-right"'); ?>
                        <div class="input-group-append">
                          <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <?php echo (!empty($error)) ? '<div class="alert alert-danger">' . $error . '</div>' : ''; ?>
                <?php echo ($this->session->flashdata('success')) ? '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>' : ''; ?>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Draft?</th>
                      <th>Category</th>
                      <th>Tags</th>
                      <th>Published Date</th>
                      <th>Created Date</th>
                      <th style="" width="15%">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($posts->count()): ?>
                      <?php
                      $i = $page + 1;
                      foreach ($posts as $post):
                        // dd($post->tags()->get(['title'])->pluck('title')->toArray());
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $post->title; ?></td>
                          <td><?php echo ($post->is_draft) ? 'Y' : 'N' ; ?></td>
                          <td><?php echo $post->category->title; ?></td>
                          <td><?php echo count($post->tags) ? '<span class="badge bg-warning">' . implode('</span><span class="badge bg-warning">', $post->tags()->get(['title'])->pluck('title')->toArray()) . '</span>' : '-'; ?></td>
                          <?php /* ?>
                          <td><?php echo strtotime($post->published_at) <= 0 ? '-' : date('d-m-Y @ h:i a', strtotime($post->published_at)); ?></td>
                          <td><?php echo strtotime($post->created_at) <= 0 ? '-' : date('d-m-Y @ h:i a', strtotime($post->created_at)); ?></td>
                          <?php //*/ ?>
                          <td><?php echo strtotime($post->published_at) <= 0 ? '-' : $post->published_at->format('d-m-Y @ h:i a'); ?></td>
                          <td><?php echo strtotime($post->created_at) <= 0 ? '-' : $post->created_at->format('d-m-Y @ h:i a'); ?></td>
                          <td>
                            <a href="<?php echo site_url($admin_base_url . 'posts/edit/' . $post->id) ?>" class="btn btn-sm btn-primary m-1"><i class="fas fa-edit"></i> <span class="d-none d-md-inline">Edit</span></a>
                            <a href="<?php echo site_url($admin_base_url . 'posts/delete/' . $post->id) ?>" onclick="return confirm('Sure to delete?')" class="btn btn-sm btn-danger m-1"><i class="fas fa-trash-alt"></i> <span class="d-none d-md-inline">Delete</span></a>
                          </td>
                        </tr>
                        <?php
                      endforeach; ?>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <?php echo $pagination; ?>
                <?php /* ?>
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
                <?php //*/ ?>
              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

  </div>
  <!-- /.content-wrapper -->
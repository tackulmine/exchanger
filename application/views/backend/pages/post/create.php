  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url($admin_base_url) ?>">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="<?php echo site_url($admin_base_url . 'posts') ?>"><?php echo plural($module_name); ?></a></li>
              <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Horizontal Form -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo $module_name; ?> Form</h3>
              </div>
              <!-- /.card-header -->

              <!-- form start -->
              <?php echo form_open_multipart($admin_base_url . 'posts/create', 'class="form-horizontal"'); ?>

                <div class="card-body">
                  <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
                  <?php echo (!empty($error)) ? '<div class="alert alert-danger">' . $error . '</div>' : ''; ?>
                  <?php echo ($this->session->flashdata('success')) ? '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>' : ''; ?>

                  <?php $this->load->view('backend/pages/post/_form'); ?>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Save</button>
                  <a href="<?php echo site_url($admin_base_url . 'posts'); ?>" class="btn btn-default float-right"><i class="fas fa-undo"></i> Cancel</a>
                </div>
                <!-- /.card-footer -->

              <?php echo form_close(); ?>
              <!-- form end -->

            </div>
            <!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

  </div>
  <!-- /.content-wrapper -->
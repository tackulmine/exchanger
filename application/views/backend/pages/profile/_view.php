<div class="form-group row">
  <label for="email" class="col-sm-2 col-form-label"><?php echo lang('edit_user_email_label'); ?></label>
  <div class="col-sm-10">
    <div class="form-text"><?php echo $email ?></div>
  </div>
</div><div class="form-group row">
  <label for="first_name" class="col-sm-2 col-form-label"><?php echo lang('edit_user_fname_label'); ?></label>
  <div class="col-sm-10">
    <div class="form-text"><?php echo $first_name ?></div>
  </div>
</div><div class="form-group row">
  <label for="last_name" class="col-sm-2 col-form-label"><?php echo lang('edit_user_lname_label'); ?></label>
  <div class="col-sm-10">
    <div class="form-text"><?php echo $last_name ?></div>
  </div>
</div><div class="form-group row">
  <label for="company" class="col-sm-2 col-form-label"><?php echo lang('edit_user_company_label'); ?></label>
  <div class="col-sm-10">
    <div class="form-text"><?php echo $company ?></div>
  </div>
</div><div class="form-group row">
  <label for="phone" class="col-sm-2 col-form-label"><?php echo lang('edit_user_phone_label'); ?></label>
  <div class="col-sm-10">
    <div class="form-text"><?php echo $phone ?></div>
  </div>
</div>
<?php /* ?>
<div class="form-group row">
  <label for="image" class="col-sm-2 col-form-label">Photo Cover</label>
  <div class="col-sm-10">
    <img id="preview_image" src="<?php echo (!empty($image)) ? "/{$image_path}{$image}" : "#" ?>" alt="" class="mb-1 img-fluid" />
    <input type="file" name="image" id="image" class="upload_image_preview" data-target="#preview_image">
  </div>
</div>
<div class="form-group row">
  <label for="bio" class="col-sm-2 col-form-label">Bio</label>
  <div class="col-sm-10">
    <textarea name="bio" id="bio" cols="30" rows="10" class="form-control editor" placeholder="" required><?php echo set_value('bio', $bio); ?></textarea>
  </div>
</div>
<div class="form-group row">
  <label for="groups" class="col-sm-2 col-form-label">Groups</label>
  <div class="col-sm-10">
    <?php echo form_dropdown('groups[]', $tag_options, set_value('groups', $groups), 'class="form-control" id="groups" multiple'); ?>
  </div>
</div>
<?php //*/ ?>
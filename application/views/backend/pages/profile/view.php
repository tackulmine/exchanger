  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url($admin_base_url) ?>">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="<?php echo site_url($admin_base_url . 'profile') ?>"><?php echo $module_name; ?></a></li>
              <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Horizontal Form -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo $module_name; ?> Detail</h3>
              </div>
              <!-- /.card-header -->

                <div class="card-body">
                  <?php echo (!empty($error)) ? '<div class="alert alert-danger">' . $error . '</div>' : ''; ?>
                  <?php echo ($this->session->flashdata('error')) ? '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>' : ''; ?>

                  <?php echo ($this->session->flashdata('success')) ? '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>' : ''; ?>

                  <?php $this->load->view('backend/pages/profile/_view'); ?>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <a href="<?php echo site_url($admin_base_url . 'profile/edit'); ?>" class="btn btn-default float-right"><i class="fas fa-edit"></i> Edit</a>
                </div>
                <!-- /.card-footer -->

            </div>
            <!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

  </div>
  <!-- /.content-wrapper -->
<div class="form-group row">
  <label for="first_name" class="col-sm-2 col-form-label"><?php echo lang('edit_user_fname_label'); ?></label>
  <div class="col-sm-10">
    <input type="text" name="first_name" id="first_name" maxlength="100" value="<?php echo set_value('first_name', $first_name); ?>" class="form-control" placeholder="" required autofocus>
  </div>
</div><div class="form-group row">
  <label for="last_name" class="col-sm-2 col-form-label"><?php echo lang('edit_user_lname_label'); ?></label>
  <div class="col-sm-10">
    <input type="text" name="last_name" id="last_name" maxlength="100" value="<?php echo set_value('last_name', $last_name); ?>" class="form-control" placeholder="">
  </div>
</div><div class="form-group row">
  <label for="company" class="col-sm-2 col-form-label"><?php echo lang('edit_user_company_label'); ?></label>
  <div class="col-sm-10">
    <input type="text" name="company" id="company" maxlength="100" value="<?php echo set_value('company', $company); ?>" class="form-control" placeholder="">
  </div>
</div><div class="form-group row">
  <label for="phone" class="col-sm-2 col-form-label"><?php echo lang('edit_user_phone_label'); ?></label>
  <div class="col-sm-10">
    <input type="text" name="phone" id="phone" maxlength="20" value="<?php echo set_value('phone', $phone); ?>" class="form-control" placeholder="">
  </div>
</div>
<?php /* ?>
<div class="form-group row">
  <label for="image" class="col-sm-2 col-form-label">Photo Cover</label>
  <div class="col-sm-10">
    <img id="preview_image" src="<?php echo (!empty($image)) ? "/{$image_path}{$image}" : "#" ?>" alt="" class="mb-1 img-fluid" />
    <input type="file" name="image" id="image" class="upload_image_preview" data-target="#preview_image">
  </div>
</div>
<div class="form-group row">
  <label for="bio" class="col-sm-2 col-form-label">Bio</label>
  <div class="col-sm-10">
    <textarea name="bio" id="bio" cols="30" rows="10" class="form-control editor" placeholder="" required><?php echo set_value('bio', $bio); ?></textarea>
  </div>
</div>
<div class="form-group row">
  <label for="groups" class="col-sm-2 col-form-label">Groups</label>
  <div class="col-sm-10">
    <?php echo form_dropdown('groups[]', $tag_options, set_value('groups', $groups), 'class="form-control" id="groups" multiple'); ?>
  </div>
</div>
<?php //*/ ?>
<div class="form-group row">
  <label for="password" class="col-sm-2 col-form-label"><?php echo lang('edit_user_password_label'); ?></label>
  <div class="col-sm-10">
    <input type="password" name="password" id="password" maxlength="20" class="form-control">
  </div>
</div>
<div class="form-group row">
  <label for="password_confirm" class="col-sm-2 col-form-label"><?php echo lang('edit_user_password_confirm_label'); ?></label>
  <div class="col-sm-10">
    <input type="password" name="password_confirm" id="password_confirm" maxlength="20" class="form-control">
  </div>
</div>
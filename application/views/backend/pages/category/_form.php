<div class="form-group row">
  <label for="title" class="col-sm-2 col-form-label">Title</label>
  <div class="col-sm-10">
    <input type="text" name="title" id="title" maxlength="100" value="<?php echo set_value('title', $title); ?>" class="form-control" placeholder="Title" required autofocus>
  </div>
</div>
<div class="form-group row">
  <label for="subtitle" class="col-sm-2 col-form-label">Subtitle</label>
  <div class="col-sm-10">
    <textarea name="subtitle" id="subtitle" cols="30" rows="3" class="form-control" placeholder="Subtitle"><?php echo set_value('subtitle', $subtitle); ?></textarea>
  </div>
</div>
<div class="form-group row">
  <label for="description" class="col-sm-2 col-form-label">Description</label>
  <div class="col-sm-10">
    <textarea name="description" id="description" cols="30" rows="10" class="form-control editor" placeholder="Description"><?php echo set_value('description', $description); ?></textarea>
  </div>
</div>
<div class="form-group row">
  <div class="offset-sm-2 col-sm-10">
    <?php /* ?>
    <div class="form-check">
      <input type="checkbox" class="form-check-input" name="is_draft" id="is_draft" value="1" <?php echo set_checkbox('is_draft', '1', (isset($is_draft) ? (bool) $is_draft : true)); ?>>
      <label class="form-check-label" for="is_draft"> is draft?</label>
    </div>
    <?php //*/ ?>
    <div class="icheck-default d-inline">
      <input type="checkbox" name="is_draft" id="is_draft" value="1" <?php echo set_checkbox('is_draft', '1', (isset($is_draft) ? (bool) $is_draft : true)); ?>>
      <label for="is_draft"> is draft?</label>
    </div>
  </div>
</div>
<?php $this->load->view('backend/pages/partials/seo_form'); ?>
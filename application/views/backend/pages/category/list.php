  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $module_name; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo site_url($admin_base_url) ?>">Dashboard</a></li>
              <li class="breadcrumb-item active"><?php echo $module_name; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?php echo $page_title; ?></h3>
                <div class="card-tools">
                  <?php echo form_open($admin_base_url . 'categories', ['method' => 'get']); ?>
                  <div class="input-group input-group-sm"
                    <?php //* ?>
                    style="width: 150px;"
                    <?php //*/ ?>
                    >
                    <?php echo form_input('keyword', $this->input->get('keyword'), 'class="form-control float-right" placeholder="Search.."'); ?>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Draft?</th>
                      <th>Post?</th>
                      <th>Created Date</th>
                      <th style="" width="15%">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($categories->count()): ?>
                      <?php
                      $i = $page + 1;
                      foreach ($categories as $category):
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $category->title; ?></td>
                          <td><?php echo ($category->is_draft) ? 'Y' : 'N' ; ?></td>
                          <td><?php echo count($category->posts) ? anchor($admin_base_url . 'posts/page?filter_category=' . $category->id, $category->posts_count) : '-'; ?></td>
                          <td><?php echo strtotime($category->created_at) <= 0 ? '-' : $category->created_at->format('d-m-Y @ h:i a'); ?></td>
                          <td>
                            <a href="<?php echo site_url($admin_base_url . 'categories/edit/' . $category->id) ?>" class="btn btn-sm btn-primary m-1"><i class="fas fa-edit"></i> <span class="d-none d-md-inline">Edit</span></a>
                            <a href="<?php echo site_url($admin_base_url . 'categories/delete/' . $category->id) ?>" onclick="return confirm('Sure to delete?')" class="btn btn-sm btn-danger m-1"><i class="fas fa-trash-alt"></i> <span class="d-none d-md-inline">Delete</span></a>
                          </td>
                        </tr>
                        <?php
                      endforeach; ?>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <?php echo $pagination; ?>
              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

  </div>
  <!-- /.content-wrapper -->
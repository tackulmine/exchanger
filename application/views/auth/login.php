    <div class="login-box">
        <div class="login-logo">
            <a href="/"><img src="/assets/img/zakycell-small-logo.png" width="100" alt="Logo"></a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">

                <div class="login-box-msg">
                  <?php /* ?>
                  <h2><?php echo lang('login_heading');?></h2>
                  <?php //*/ ?>
                  <p><?php echo lang('login_subheading');?></p>
                </div>

                <?php echo !empty($message) ? '<div class="alert alert-warning">' . $message . '</div>' : ""; ?>

                <?php echo form_open('deminz/auth/login'); ?>
                    <div class="input-group mb-3">
                        <?php //echo lang('login_identity_label', 'identity');?>
                        <?php echo form_input($identity);?>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <?php //echo lang('login_password_label', 'password');?>
                        <?php echo form_input($password);?>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                              <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                                <label for="remember">
                                    <?php echo lang('login_remember_label', 'remember');?>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-primary btn-block"');?>
                        </div>
                        <!-- /.col -->
                    </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
<div class="blank" style="padding-top: 100px;padding-bottom: 100px; background: #596065;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">

                            <?php if ($this->session->flashdata('success')): ?>
                                STATUS PESANAN MU
                            <?php else: ?>
                                BUAT PESANAN MU SEKARANG!
                            <?php endif; ?>
                        </h4>
                    </div>
                    <div class="panel-body">

                        <?php if ($this->session->flashdata('success')): ?>

                            <?php echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div><div class="text-right"><a href="' . site_url() . '" class="btn btn-sm btn-default"><i class="fa fa-home"></i> Home</a></div>'; ?>

                        <?php else: ?>

                            <?php
                            if (validation_errors()) {
                                echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
                            } ?>
                            <?php
                            if (!empty($error)) {
                                echo '<div class="alert alert-danger">' . $error . '</div>';
                            } ?>

                            <?php echo form_open('order', ['data-toggle' => 'validator']); ?>
                                <input type="hidden" name="order_recaptcha" id="order_recaptcha">
                                <div class="form-group">
                                    <label for="first_name">Nama Depan <strong class="text-danger">*</strong></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo set_value('first_name') ?>" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Nama Belakang</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo set_value('last_name') ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <strong class="text-danger">*</strong></label>
                                    <input type="email" class="form-control" id="email" name="email" value="<?php echo set_value('email') ?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Nomor Telepon/HP <strong class="text-danger">*</strong></label>
                                    <input type="tel" class="form-control" id="phone" name="phone" value="<?php echo set_value('phone') ?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="telegram">Telegram</label>
                                    <input type="text" class="form-control" id="telegram" name="telegram" value="<?php echo set_value('telegram') ?>">
                                </div>
                                <div class="form-group">
                                    <label for="layanan">Pilih Layanan <strong class="text-danger">*</strong></label>
                                    <?php
                                    $options = [
                                        '' => '---',
                                        'Withdraw Indodax' => 'Withdraw Indodax',
                                        'Deposit Indodax' => 'Deposit Indodax',
                                        'Escrow' => 'Escrow',
                                        'Isi Pulsa' => 'Isi Pulsa',
                                        'Jual/Beli Crypto' => 'Jual/Beli Crypto',
                                        'Paypal' => 'Paypal',
                                        'Payeer' => 'Payeer',
                                        'Skrill' => 'Skrill',
                                    ];
                                    echo form_dropdown('layanan', $options, set_value('layanan'), 'class="form-control" id="layanan" required'); ?>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="agreement" value="1" required>
                                            Saya sudah membaca dan setuju dengan <a href="#!" data-toggle="modal" data-target="#termModal" data-backdrop="static">syarat dan ketentuan</a> <strong class="text-danger">*</strong>
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-default">Pesan Sekarang</button>
                            <?php echo form_close(); ?>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

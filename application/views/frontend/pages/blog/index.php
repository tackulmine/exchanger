<div class="blog-wrapper">

  <section class="intro">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <h2 class="h3"><?php echo $page_title; ?></h2>
          <?php if (!empty($subtitle)): ?>
            <p class="lead"><?php echo $subtitle; ?></p>
          <?php endif ?>
        </div>
      </div>
    </div>
  </section>

  <div class="container">
    <div class="row">
      <!-- Latest Posts -->
      <main class="posts-listing col-md-8">
        <div class="row">
        <?php if ($posts->count()): ?>
          <?php foreach ($posts as $post): ?>
            <!-- post -->
            <div class="post col-lg-6">
              <div class="post-thumbnail">
                <a href="<?php echo $post->url; ?>">
                  <img src="/<?php echo image($post->image_path . $post->image, 'wide'); ?>" alt="<?php echo $post->title; ?>" class="img-fluid">
                </a>
              </div>
              <div class="post-details">
                <div class="post-meta d-flex justify-content-between">
                  <?php /* ?>
                  <div class="date meta-last"><?php echo $post->published_at->format('d M Y @ H:i'); ?></div>
                  <?php //*/ ?>
                  <div class="date meta-last"><?php echo $post->published_at->format('d M Y'); ?></div>
                  <div class="category"><a href="<?php echo $post->category->url; ?>"><?php echo $post->category->title; ?></a></div>
                </div>
                <a href="<?php echo $post->url; ?>">
                  <h3 class="h4"><?php echo $post->title; ?></h3>
                </a>
                <p class="text-muted"><?php echo $post->subtitle; ?></p>
                <?php //* ?>
                <footer class="post-footer d-flex align-items-center">
                  <?php /* ?>
                  <a href="#!" class="author d-flex align-items-center flex-wrap">
                    <div class="avatar"><img src="img/avatar-3.jpg" alt="..." class="img-fluid"></div>
                    <div class="title"><span>Admin</span></div>
                  </a>
                  <?php //*/ ?>
                  <div class="title"><i class="fas fa-user"></i> Admin</div>
                  <div class="views meta-last"><i class="fas fa-eye"></i> <?php echo $post->visited_count; ?></div>
                  <?php /* ?>
                  <div class="date"><i class="icon-clock"></i> <?php echo $post->published_at->format('d M Y @ H:i'); ?></div>
                  <div class="comments meta-last"><i class="icon-comment"></i>12</div>
                  <?php //*/ ?>
                </footer>
              <?php //*/ ?>
              </div>
            </div>
          <?php endforeach ?>
        <?php endif; ?>
        </div>
        <!-- Pagination -->
        <?php /* ?>
        <nav aria-label="Page navigation example">
          <ul class="pagination pagination-template d-flex justify-content-center">
            <li class="page-item"><a href="#" class="page-link"> <i class="fa fa-angle-left"></i></a></li>
            <li class="page-item"><a href="#" class="page-link active">1</a></li>
            <li class="page-item"><a href="#" class="page-link">2</a></li>
            <li class="page-item"><a href="#" class="page-link">3</a></li>
            <li class="page-item"><a href="#" class="page-link"> <i class="fa fa-angle-right"></i></a></li>
          </ul>
        </nav>
        <?php //*/ ?>
        <?php echo $pagination; ?>
      </main>

      <?php $this->load->view('frontend/pages/blog/aside'); ?>
    </div>
  </div>

</div>
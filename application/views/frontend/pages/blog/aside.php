      <aside class="col-md-4">
        <!-- Widget [Search Bar Widget]-->
        <div class="widget search">
          <header>
            <h3 class="h4">Temukan di Blog <?php echo APP_NAME; ?></h3>
          </header>
          <form action="<?php echo site_url('blog') ?>" method="get" class="search-form">
            <div class="input-group">
              <input type="search" name="search" id="search" class="form-control" placeholder="Apa yang sedang kamu cari?">
              <span class="input-group-btn">
                <button type="submit" class="btn submit"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
        </div>

        <?php if ($this->uri->segment(2) != "blog"): ?>
          <?php //* ?>
          <?php if ($sidebar_posts->count()): ?>
          <!-- Widget [Latest Posts Widget] -->
          <div class="widget latest-posts">
            <header>
              <h3 class="h4">Artikel Terbaru</h3>
            </header>
            <div class="blog-posts">
              <?php foreach ($sidebar_posts as $post): ?>
              <a href="<?php echo $post->url; ?>">
                <div class="item d-flex align-items-center">
                  <div class="image"><img src="/<?php echo image($post->image_path . $post->image, 'xsmall_square'); ?>" alt="<?php echo $post->title; ?>" class="img-fluid"></div>
                  <div class="title"><strong><?php echo $post->title; ?></strong>
                    <div class="d-flex align-items-center">
                      <?php /* ?>
                      <div class="views"><i class="fa fa-eye"></i> <?php echo $post->visited_count; ?></div>
                      <div class="comments"><i class="fa fa-comment"></i>12</div>
                      <?php //*/ ?>
                      <div class="comments"><i class="fa fa-eye"></i> <?php echo $post->visited_count; ?></div>
                    </div>
                  </div>
                </div>
              </a>
              <?php endforeach ?>
            </div>
          </div>
          <?php endif; ?>
          <?php //*/ ?>
        <?php endif ?>

        <?php if ($sidebar_categories->count()): ?>
          <!-- Widget [Categories Widget] -->
          <div class="widget categories">
            <header>
              <h3 class="h4">Kategori</h3>
            </header>
            <?php foreach ($sidebar_categories as $category): ?>
              <?php /* ?>
              <div class="item d-flex justify-content-between"><a href="<?php echo $category->url; ?>"><?php echo $category->title ?></a><span><?php echo $category->posts()->published()->count(); ?></span></div>
              <?php //*/ ?>
              <div class="item d-flex justify-content-between"><a href="<?php echo $category->url; ?>"><?php echo $category->title ?></a><span><?php echo $category->posts_count; ?></span></div>
            <?php endforeach ?>
          </div>
        <?php endif ?>

        <?php if ($sidebar_tags->count()): ?>
          <!-- Widget [Tags Cloud Widget]-->
          <div class="widget tags">
            <header>
              <h3 class="h4">Topik</h3>
            </header>
            <ul class="list-inline">
              <?php foreach ($sidebar_tags as $tag): ?>
                <li class="list-inline-item"><a href="<?php echo $tag->url; ?>" class="tag">#<?php echo $tag->title; ?></a></li>
              <?php endforeach ?>
            </ul>
          </div>
        </aside>
      <?php endif ?>
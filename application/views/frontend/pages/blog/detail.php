<div class="blog-wrapper">

  <div class="container">
    <div class="row">
      <!-- Latest Posts -->
      <main class="post blog-post col-lg-8">
        <div class="post-single">
          <div class="post-thumbnail"><img src="/<?php echo image($post->image_path . $post->image, 'large'); ?>" alt="<?php echo $post->title; ?>" class="img-responsive"></div>
          <div class="post-details">

            <div class="post-meta d-flex justify-content-between">
              <div class="category"><a href="<?php echo $post->category->url; ?>"><?php echo $post->category->title; ?></a></div>
            </div>

            <h1>
              <?php echo $post->title; ?>
              <?php /* ?><a href="#"><i class="fa fa-bookmark-o"></i></a><?php //*/ ?>
            </h1>

            <div class="post-footer d-flex align-items-center flex-column flex-sm-row">
              <?php /* ?>
              <a href="#" class="author d-flex align-items-center flex-wrap">
                <div class="avatar"><img src="img/avatar-1.jpg" alt="..." class="img-fluid"></div>
                <div class="title"><span>John Doe</span></div>
              </a>
              <?php //*/ ?>
              <div class="d-flex align-items-center flex-wrap">
                <div class="title"><i class="fa fa-user"></i> Admin</div>
                <div class="date"><i class="fa fa-clock"></i> <?php echo $post->published_at->format('d M Y'); ?></div>
                <div class="views meta-last"><i class="fa fa-eye"></i> <?php echo $post->visited_count; ?></div>
                <?php /* ?>
                <div class="comments meta-last"><i class="icon-comment"></i>12</div>
                <?php //*/ ?>
              </div>
            </div>

            <div class="post-body">
              <p class="lead"><?php echo $post->subtitle; ?></p>

              <?php echo $post->content_html; ?>
            </div>

            <?php /* ?>
            <div class="post-tags"><a href="#" class="tag">#Business</a><a href="#" class="tag">#Tricks</a><a href="#" class="tag">#Financial</a><a href="#" class="tag">#Economy</a></div>
            <?php //*/ ?>

            <?php //dd($post->tags_count); ?>
            <?php if ($post->tags_count): ?>
              <div class="post-tags">
                <?php //dd($post->tags); ?>
                <?php foreach ($post->tags as $tag): ?>
                  <a href="<?php echo $tag->url; ?>" class="tag">#<?php echo $tag->title; ?></a>
                <?php endforeach ?>
              </div>
            <?php endif ?>

            <div class="posts-nav d-flex justify-content-between align-items-stretch flex-column flex-md-row">
              <?php if ($older_post): ?>
                <a href="<?php echo $older_post->url; ?>" class="prev-post text-left d-flex align-items-center">
                  <div class="icon prev"><i class="fa fa-angle-left"></i></div>
                  <div class="text"><strong class="text-primary">Previous Post </strong>
                    <h5><?php echo $older_post->title ?></h5>
                  </div>
                </a>
              <?php endif; ?>
              <?php if ($newer_post): ?>
                <a href="<?php echo $newer_post->url ?>" class="next-post text-right d-flex align-items-center justify-content-end">
                  <div class="text"><strong class="text-primary">Next Post </strong>
                    <h5><?php echo $newer_post->title; ?></h5>
                  </div>
                  <div class="icon next"><i class="fa fa-angle-right"></i></div>
                </a>
              <?php endif; ?>
            </div>

            <?php //$this->load->view('frontend/pages/blog/comment'); ?>

          </div>
        </div>
      </main>

      <?php $this->load->view('frontend/pages/blog/aside'); ?>

    </div>
  </div>

</div>
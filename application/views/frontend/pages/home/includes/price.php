<!-- API HARGA https://coinmarketcap.com/api/pricing/ atau https://min-api.cryptocompare.com/-->
<section id="price">
    <div class="container">
        <div class="section-heading">
            <h4>Crypto Marketcap</h4>
            Pergerakan harga crypto terbaru hari ini (LIVE)
            <div class="line-dec line-center"></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <!--kalau bisa untuk yang ini crawl data harga saat ini, harga 1 jam, harga 24 jam-->
                        <th class="text-center">Koin</th>
                        <th class="text-center">Harga</th>
                        <!-- <th class="text-center">Volume 24 Jam</th> -->

                        <th class="text-center">1 Jam</th>
                        <th class="text-center">24 Jam</th>
                        <th class="text-center">7 Hari</th>
                        <th class="text-center">Market Cap</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach ($zaky_coingecko_coin_markets as $key => $values): ?>
                    <tr>
                        <td class="text-left">
                            <img width="20"
                            src="<?php echo $coingecko_coin_markets[$key]['image']; ?>"
                            alt="<?php echo $coingecko_coin_markets[$key]['name']; ?>">
                            <?php echo $coingecko_coin_markets[$key]['name']; ?>
                        </td>
                        <td class="text-right"><?php echo rupiah($coingecko_coin_markets[$key]['current_price']); ?></td>
                        <td><span class="<?php echo $coingecko_coin_markets[$key]['price_change_percentage_1h_in_currency'] < 0 ? "text-danger" : "text-success"; ?>">
                            <?php echo percent($coingecko_coin_markets[$key]['price_change_percentage_1h_in_currency']); ?>%
                            </span>
                        </td>
                        <td><span class="<?php echo $coingecko_coin_markets[$key]['price_change_percentage_24h'] < 0 ? "text-danger" : "text-success"; ?>"><?php echo percent($coingecko_coin_markets[$key]['price_change_percentage_24h']); ?>%</span></td>

                        <td><span class="<?php echo $coingecko_coin_markets[$key]['price_change_percentage_7d_in_currency'] < 0 ? "text-danger" : "text-success"; ?>"><?php echo percent($coingecko_coin_markets[$key]['price_change_percentage_7d_in_currency']); ?>%</span></td>
                        <td class="text-right"><?php echo 'Rp ' . $zaky_coingecko_coin_markets[$key]['market_cap_short']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        Crypto marketcap data dari <a href="http://coingecko.com/ref=zaky-cell.com" target="_blank" color="#ffffff"><strong>Coingecko</strong></a> - (refresh data tiap 1 menit)
    </div>
</section>

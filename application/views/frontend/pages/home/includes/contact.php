    <div id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h4>Hubungi kami</h4>
                        <div class="line-dec line-center"></div>
                        <p>Apabila Anda memiliki kendala apapun untuk bertransaksi di Zaky Cell atau ingin bekerja sama dengan kami, silakan hubungi kami di:</p>
                        <div class="pop-button">
                            <h4>Hubungi Kami!</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="pop">
                        <span>✖</span>
                        <?php echo form_open('contact', ['id' => 'contact']); ?>
                            <input type="hidden" name="contact_recaptcha" id="contact_recaptcha">
                            <div class="row">
                                <div class="col-md-12">
                                  <fieldset>
                                    <input name="contact_name" id="contact_name" type="text" class="form-control" placeholder="Nama Anda..." required>
                                  </fieldset>
                                </div>
                                <div class="col-md-12">
                                  <fieldset>
                                    <input name="contact_email" id="contact_email" type="email" class="form-control" placeholder="Email Anda..." required>
                                  </fieldset>
                                </div>
                                <div class="col-md-12">
                                  <fieldset>
                                    <textarea name="contact_message" id="contact_message" rows="6" class="form-control" placeholder="Tulis pesan Anda..." required></textarea>
                                  </fieldset>
                                </div>
                                <div class="col-md-12">
                                  <fieldset>
                                    <button type="submit" id="form-submit" class="btn"><i class="fab fa-telegram fa-lg"></i> Kirim pesan</button>
                                  </fieldset>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="orderModal">
    <div class="modal-dialog">
        <?php echo form_open('order', ['data-toggle' => 'validator']); ?>
        <input type="hidden" name="order_recaptcha" id="order_recaptcha">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">BUAT PESANAN MU SEKARANG!</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="first_name">Nama Depan
                        <strong class="text-danger">*</strong>
                    </label>
                    <input type="text" class="form-control" id="first_name" name="first_name" required autofocus>
                </div>
                <div class="form-group">
                    <label for="last_name">Nama Belakang
                    </label>
                    <input type="text" class="form-control" id="last_name" name="last_name">
                </div>
                <div class="form-group">
                    <label for="email">Email
                        <strong class="text-danger">*</strong>
                    </label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div class="form-group">
                    <label for="phone">Nomor Telepon/HP
                        <strong class="text-danger">*</strong>
                    </label>
                    <input type="tel" class="form-control" id="phone" name="phone" required>
                </div>
                <div class="form-group">
                    <label for="telegram">Telegram
                    </label>
                    <input type="text" class="form-control" id="telegram" name="telegram">
                </div>
                <div class="form-group">
                    <label for="layanan">Pilih Layanan
                        <strong class="text-danger">*</strong>
                    </label>
                    <select class="form-control" id="layanan" name="layanan" required>
                        <option value="">- Pilih Layanan -</option>
                        <option value="Withdraw Indodax">Withdraw Indodax</option>
                        <option value="Deposit Indodax">Deposit Indodax</option>
                        <option value="Escrow">Escrow</option>
                        <option value="Isi Pulsa">Isi Pulsa</option>
                        <option value="Jual/Beli Crypto">Jual/Beli Crypto</option>
                        <option value="Paypal">Paypal</option>
                        <option value="PerfectMoney">PerfectMoney</option>
                        <option value="Payeer">Payeer</option>
                        <option value="Skrill">Skrill</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="agreement" value="1" required>
                            Saya sudah membaca dan setuju dengan <a href="#!" data-toggle="modal" data-target="#termModal" data-backdrop="static">syarat dan ketentuan</a> <strong class="text-danger">*</strong>
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php //* ?>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php //*/ ?>
                <button type="reset" class="btn btn-default">Reset form</button>
                <button type="submit" class="btn btn-primary">Pesan Sekarang</button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<section id="testimonial">
    <div class="container">
    <div class="section-heading">
        <h4>TESTIMONIAL PELANGGAN</h4>
        <div class="line-dec line-center"></div>
    </div><br>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div id="owl-testimonials" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonials-item">
                            <p>“ Withdraw berjalan dengan lancar. Terima kasih banyak atas transaksi yang baik dan proses WD yang cepat.”</p>
                            <h4>masulum</h4>
                            <span>Order: WD Bitcoin & XLM ke IDR</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonials-item">
                            <p>“ alhamdulillah gan kosnya banyak suka kami anak bitcoin sempol leces udah banyak yang makai and juga trendy ok bang zakky... law ada model terbaru jangan lupa kabarin kami yang di leces.....”</p>
                            <h4>Ojengonggu</h4>
                            <span>Order: Kaos custom</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonials-item">
                            <p>“ Rekomended banget nih, pelayanan bagus orangnya ramah. Proses Pencairan voucher VIP lumayan cepat. Terimakasih juga untuk kaosnya bahan bagus, lembut dan gak panas. Semoga Sukses Terus gan.”</p>
                            <h4>M. Sidik</h4>
                            <span>Order: WD Voucher & Kaos custom</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonials-item">
                            <p>“ Makasih saya langsung chat di WA agan dan transaksi saya yang jual LTC prosesnya sangat cepat. Yang lebih luar biasanya dari transaksi dengan agan yaitu ketika saya sudah send LTCnya trus saya check di blockchain tapi blom ada konfirmasinya satupun. Dan uangnya langsung dikirim ke bank saya.”</p>
                            <h4>Saoda</h4>
                            <span>Order: WD Litecoin & Voucher</span>
                        </div>
                    </div>
                    <!-- Nambah testing nang duwure iki -->
                    <!--<div class="item">
                        <div class="testimonials-item">
                            <p>“ Maecenas eu odio pharetra, elementum lorem eget, efficitur erat. Duis eget justo non nisi iaculis vestibulum. Aliquam erat volutpat. ”</p>
                            <h4>Richard Beal</h4>
                            <span>Senior Developer</span>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>

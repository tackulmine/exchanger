<div class="parallax-content baner-content" id="home">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7" style="border-right: 1px solid #efefef;">
                <div class="text-content">
                    <p><img src="/assets/img/zakycell-small-logo.png" height="50"></img></p>
                    <h2>Zaky Cell</h2>
                    <p>Zaky Cell adalah jasa untuk penarikan Indodax, Deposit ke Indodax dan melayani escrow transaksi dengan aman. Zaky Cell merupakan partner resmi Indodax. Kami juga menyediakan jasa Kaos komunitas crypto satuan dengan custom desain</p>
                    <div class="primary-white-button">
                        <a href="#" class="scroll-link" data-id="about">Pelajari Lebih Lanjut</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-content" style="margin-top: 85px">
                    <h3 style="color:white;">Mitra Resmi</h3>
                    <p><a href="https://indodax.com/ref/AakZaki/1"><img src="/assets/img/indodax-logo.png" height="80"></img></a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="parallax-content baner-content" id="home">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7" style="border-right: 1px solid #efefef;">
                <div class="text-content">
                    <p><img src="/assets/img/zakycell-small-logo.png" height="50"></img></p>
                    <h2>Zaky Cell</h2>
                    <p>Zaky Cell adalah jasa untuk withdraw Indodax, Deposit ke Indodax, jual beli Bitcoin, Ethereum, Altcoin crypto lainnya dan melayani escrow transaksi dengan aman. Zaky Cell merupakan partner resmi Indodax. Kami juga menyediakan jasa kaos komunitas crypto satuan dengan custom desain. Kami juga melayani layanan jasa deposit PayPal, Payeer dan Skrill. Untuk meningkatkan kualitas layanan Zaky Cell, kami juga telah menjadi mitra resmi perdagangan <a href="https://p2p.binance.com/id/marchantDetail/097b8c1d641044c984220ef9b946db76" target="_blank">P2P Binance</a> sebagai salah satu merchant Indonesia yang sudah terverifikasi.</p>
                    <div class="primary-white-button">
                        <a href="#" class="scroll-link" data-id="about">Pelajari Lebih Lanjut</a> <a href="https://wa.me/6282330718333" target="_blank"><i class="fab fa-whatsapp"></i></a> <a href="https://t.me/AakZaki" target="_blank"><i class="fab fa-telegram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-content" style="margin-top: 85px">
                    <h3 style="color:white;"><u>Mitra Resmi</u></h3>
                    <p><a href="https://indodax.com/ref/AakZaki/1"><img src="/assets/img/indodax-logo-new.png" height="60"></img></a></p>
                    <p><a href="https://p2p.binance.com/id/marchantDetail/097b8c1d641044c984220ef9b946db76"><img src="/assets/img/binance-p2p.png" height="60"></img></a></p>
                </div>
            </div>
        </div>
    </div>
</div>

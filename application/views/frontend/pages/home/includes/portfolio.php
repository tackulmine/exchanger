<section id="portfolio">
    <div class="content-wrapper">
        <div class="inner-container container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="section-heading">
                        <h4>Merchandise Zaky Cell</h4>
                        <div class="line-dec"></div>
                        <p>Ingin membuat kaos custom untuk komunitas Anda? Ini adalah contoh hasil desain kami.</p>
                        <?php /* ?>
                        <div class="filter-categories">
                            <ul class="project-filter">
                                <li class="filter active" data-filter="all"><span>Show All</span></li>
                                <li class="filter" data-filter="branding"><span>Branding</span></li>
                                <li class="filter" data-filter="graphic"><span>Graphic</span></li>
                                <li class="filter" data-filter="nature"><span>Nature</span></li>
                                <li class="filter" data-filter="animation"><span>Animation</span></li>
                            </ul>
                        </div>
                        <?php //*/ ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="projects-holder-3">
                        <div class="projects-holder">
                            <div class="row">
                                <?php for($i=1;$i<5;$i++): ?>
                                <div class="col-md-6 col-sm-6 project-item mix kaos mix_all" style="display: inline-block;  opacity: 1;">
                                  <div class="thumb">
                                        <div class="image">
                                            <a href="/assets/img/products/kaos<?php echo $i; ?>.png" data-lightbox="image-1"><img width="300" src="/assets/img/products/kaos<?php echo $i; ?>.png"></a>
                                        </div>
                                  </div>
                                </div>
                                <?php endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
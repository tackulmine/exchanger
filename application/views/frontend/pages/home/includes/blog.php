<div class="tabs-content" id="blog">
    <div class="container">
        <div class="row">
            <div class="wrapper">
                <div class="col-md-4">
                    <div class="section-heading">
                        <h4>Blog <?php echo APP_NAME; ?></h4>
                        <div class="line-dec"></div>
                        <p>Silakan membaca Blog <?php echo APP_NAME; ?> untuk informasi kita bersama.</p>
                        <?php if ($posts->count()): ?>
                            <ul class="tabs clearfix" data-tabgroup="first-tab-group">
                                <?php $i = 1; ?>
                                <?php foreach ($posts as $post): ?>
                                    <?php //var_dump($post->title); die; ?>
                                    <li><a href="#tab<?php echo $i; ?>"<?php echo $i == 1 ? ' class="active"' : ''; ?>><?php echo $post->title; ?></a></li>
                                    <?php $i++; ?>
                                <?php endforeach ?>
                            </ul>
                        <?php else: ?>
                            <ul class="tabs clearfix" data-tabgroup="first-tab-group">
                                <li><a href="#tab1" class="active">Nulla eget convallis augue</a></li>
                                <li><a href="#tab2">Quisque ultricies maximus</a></li>
                                <li><a href="#tab3">Sed vel elit et lorem</a></li>
                                <li><a href="#tab4">Vivamus purus neque</a></li>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <section id="first-tab-group" class="tabgroup">
                        <?php if ($posts->count()): ?>
                            <?php $i = 1; ?>
                            <?php foreach ($posts as $post): ?>
                                <div id="tab<?php echo $i; ?>"<?php echo $i == 1 ? ' style="display: block;"' : ' style="display: none;"'; ?>>
                                    <a href="<?php echo $post->url; ?>"><img src="/<?php echo image($post->image_path . $post->image, 'wide'); ?>" alt="<?php echo $post->title; ?>"></a>
                                    <div class="text-content">
                                        <h4><a href="<?php echo $post->url; ?>"><?php echo $post->title; ?></a></h4>
                                        <span><a href="#!"><?php echo $post->category->title; ?></a> / Admin / <?php echo $post->published_at->format('d F Y'); ?></span>
                                        <p><?php echo $post->subtitle; ?></p>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php endforeach ?>
                        <?php else: ?>
                            <div id="tab1" style="display: block;">
                                <img src="/assets/img/blog_item_01.jpg" alt="">
                                <div class="text-content">
                                    <h4>Nulla eget convallis augue</h4>
                                    <span><a href="#">Digital Marketing</a> / <a href="#">Honey</a> / <a href="#">21 September 2020</a></span>
                                    <p>Donec interdum scelerisque auctor. Nulla id lorem auctor, bibendum lectus elementum, porta felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                                </div>
                            </div>
                            <div id="tab2" style="display: none;">
                                <img src="/assets/img/blog_item_02.jpg" alt="">
                                <div class="text-content">
                                    <h4>Quisque ultricies maximus</h4>
                                    <span><a href="#">Branding</a> / <a href="#">David</a> / <a href="#">24 August 2020</a></span>
                                    <p>Etiam fringilla posuere pretium. Maecenas tempor pellentesque elit in dapibus. Curabitur viverra urna sem, ut sollicitudin sem congue vel. Donec fringilla augue in justo molestie fermentum quis ac mi.</p>
                                </div>
                            </div>
                            <div id="tab3" style="display: none;">
                                <img src="/assets/img/blog_item_03.jpg" alt="">
                                <div class="text-content">
                                    <h4>Sed vel elit et lorem</h4>
                                    <span><a href="#">Web Design</a> / <a href="#">William</a> / <a href="#">18 July 2020</a></span>
                                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id ipsum porta, dictum sem sed, bibendum quam. Maecenas mattis risus eget orci rhoncus.</p>
                                </div>
                            </div>
                            <div id="tab4" style="display: none;">
                                <img src="/assets/img/blog_item_04.jpg" alt="">
                                <div class="text-content">
                                    <h4>Vivamus purus neque</h4>
                                    <span><a href="#">E-Commerce</a> / <a href="#">George</a> / <a href="#">14 July 2020</a></span>
                                    <p>Aliquam erat volutpat. Nulla at nunc nec ante rutrum congue id in diam. Nulla at lectus non turpis placerat volutpat lacinia ut mi. Quisque ultricies maximus justo a blandit. Donec sit amet commodo arcu. Sed sit amet iaculis mi, vel fermentum nisi. Morbi dui enim, vestibulum non accumsan ac, tempor non nisl.</p>
                                </div>
                            </div>
                        <?php endif; ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

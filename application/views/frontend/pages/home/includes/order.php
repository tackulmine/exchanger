<!--butuh css tambahan (id pesan)-->
<section id="pesan" class="page-section">
    BUAT PESANAN MU SEKARANG!
    <div class="container">
        <form action="/assets/signup" method="post">
            <p>
                <label>Nama Depan</label><br>
                <input type="text" name="first_name">
            </p>
            <p>
                <label>Naman Belakang</label><br>
                <input type="text" name="last_name">
            </p>
            <p>
                <label>Email</label><br>
                <input type="email" name="email" required>
            </p>
            <p>
                <label>Nomor Telepon/HP</label><br>
                <input type="tel" name="phone">
            </p>
            <p>
                <label>Telegram</label><br>
                <input type="text" name="last_name">
            </p>
            <p>
                <label>Pilih Layanan</label><br>
                <select>
                    <option>Withdraw Indodax</option>
                    <option>Deposit Indodax</option>
                    <option>Escrow</option>
                    <option>Isi Pulsa</option>
                    <option>Jual/Beli Crypto</option>
                    <option>Paypal</option>
                    <option>PerfectMoney</option>
                    <option>Payeer</option>
                    <option>Skrill</option>
                </select>
            </p>
            <p>
                <label>
                    <input type="checkbox" value="terms">
                    I agree to the <a href="/assets/terms">terms and conditions</a>
                </label>
            </p>
            <p>
                <button>Pesan Sekarang</button>
                <button type="reset">Reset form</button>
            </p>
        </form>
    </div>
</section>

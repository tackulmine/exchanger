<section id="service">
    <div class="container">
        <div class="section-heading">
            <h4>DAFTAR HARGA LAYANAN</h4>
            <div class="line-dec line-center"></div>
        </div><br>

        <h5>JUAL/BELI CRYPTO</h5><br>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">KOIN</th>
                        <th class="text-center">JUAL</th>
                        <th class="text-center">BELI</th>
                        <th class="text-center">PESAN</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($indodax_tickers as $key => $values): ?>
                    <?php if ($key !== "usdt_idr"): ?>
                        <tr>
                            <td class="text-left">
                                <img width="20"
                                src="<?php echo $coingecko_coin_markets[$values['symbol']]['image']; ?>"
                                alt="<?php echo $values['name']; ?>"
                                >
                                <?php echo $values['name']; ?>
                            </td>
                            <td class="text-right"><?php echo rupiah($values['zaky_sell']); ?></td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td class="text-left"><img width="20" src="/assets/img/paypal.png" alt="Paypal"> Paypal</td>
                            <td class="text-right">-</td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><img width="20" src="/assets/img/payeer.png" alt="Payeer"> Payeer</td>
                            <td class="text-right">-</td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left"><img width="20" src="/assets/img/skrill.png" alt="Skrill"> Skrill</td>
                            <td class="text-right">-</td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>

                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
            * Harga yang tercantum memiliki interval waktu 15 menit untuk refresh. Untuk mengetahui harga real-time, silakan <a href="https://t.me/AakZaki" target="_blank">hubungi kami</a>
        </div><br>

        <h5>LAYANAN</h5><br>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Jenis Layanan</th>
                        <th class="text-center">Nominal</th>
                        <th class="text-center">Biaya</th>
                        <th class="text-center">Order</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Deposit Indodax</td>
                        <td>Limit Rp100.000.000</td>
                        <td>Gratis</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Withdraw Indodax</td>
                        <td>≤ Rp1.999.999</td>
                        <td>Rp10.000*</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Withdraw Indodax</td>
                        <td> Rp2.000.000 - Rp3.999.999</td>
                        <td>Rp20.000*</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Withdraw Indodax</td>
                        <td> ≥ Rp4.000.000</td>
                        <td><b>0.4%</b></td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Escrow</td>
                        <td>< Rp1.000.000</td>
                            <td>Rp10.000</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Escrow</td>
                        <td>> Rp1.000.000</td>
                        <td>1%</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>


                </tbody>
            </table>
            <font color="#f7c552">* Fee yang tertera tidak termasuk biaya admin transfer antar bank yang akan disesuaikan. <br/><b>Gratis biaya admin</b> transfer antar bank untuk pelanggan yang melakukan penarikan minimum 25 juta</font><br/>
            <font color="#12FAD3">* Fee dapat berubah sewaktu-waktu tanpa pemberitahuan. Silakan tanyakan terlebih dahulu untuk fee terbaru.</font>
        </div><br>
    </div>
</section>

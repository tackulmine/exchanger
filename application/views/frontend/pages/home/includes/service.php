<section id="service">
    <div class="container">
        <div class="section-heading">
            <h4>DAFTAR HARGA LAYANAN</h4>
            <div class="line-dec line-center"></div>
        </div><br>

        <h5>JUAL/BELI CRYPTO</h5><br>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">KOIN</th>
                        <th class="text-center">JUAL</th>
                        <th class="text-center">BELI</th>
                        <th class="text-center">PESAN</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($indodax_tickers as $key => $values): ?>
                    <?php if ($key !== "usdt_idr"): ?>
                        <tr>
                            <td class="text-left">
                                <img width="20"
                                src="<?php echo $coingecko_coin_markets[$values['symbol']]['image']; ?>"
                                alt="<?php echo $values['name']; ?>"
                                >
                                <?php echo $values['name']; ?>
                            </td>
                            <td class="text-right"><?php echo rupiah($values['zaky_sell']); ?></td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td class="text-left"><img width="20" src="/assets/img/paypal.png" alt="Paypal"> Paypal</td>
                            <td class="text-right">-</td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left"><img width="20" src="/assets/img/perfect-money-png-4.png" alt="PerfectMoney"> PerfectMoney</td>
                            <td class="text-right">-</td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left"><img width="20" src="/assets/img/payeer.png" alt="Payeer"> Payeer</td>
                            <td class="text-right">-</td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left"><img width="20" src="/assets/img/skrill.png" alt="Skrill"> Skrill</td>
                            <td class="text-right">-</td>
                            <td class="text-right"><?php echo rupiah($values['zaky_buy']); ?></td>
                            <td><a href="#!"
                                data-toggle="modal"
                                data-target="#orderModal"
                                data-backdrop="static">
                                PESAN
                                </a>
                            </td>
                        </tr>
                        
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div><br>

        <h5>LAYANAN</h5><br>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Jenis Layanan</th>
                        <th class="text-center">Nominal</th>
                        <th class="text-center">Biaya</th>
                        <th class="text-center">Order</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Deposit Indodax</td>
                        <td>Limit IDR 100.000.000</td>
                        <td>Gratis</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Withdraw Indodax</td>
                        <td>< 1.000.000</td>
                        <td>10.000</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Withdraw Indodax</td>
                        <td>> Rp1.000.000</td>
                        <td>1%</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Escrow</td>
                        <td>
                            < 1.000.000</td>
                            <td>10.000
                        </td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Escrow</td>
                        <td>>1.000.000</td>
                        <td>1%</td>
                        <td><a href="#!"
                            data-toggle="modal"
                            data-target="#orderModal"
                            data-backdrop="static">
                            PESAN
                            </a>
                        </td>
                    </tr>


                </tbody>
            </table>
        </div><br>
    </div>
</section>

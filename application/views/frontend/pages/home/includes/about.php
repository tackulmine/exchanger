<section id="about" class="page-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/money.png" alt="Withdraw Indodax">
                    </div>
                    <h4>Withdraw Indodax</h4>
                    <div class="line-dec"></div>
                    <p>Zaky Cell memberikan kemudahan kepada Anda yang ingin menarik uang dari Indodax ke rekening bank Anda dengan cepat. Proses akan dilakukan setelah Anda mengirimkan kode voucher. Waktu rata-rata yang diperlukan untuk transfer ke rekening Anda adalah 1 menit setelah voucher Anda kami gunakan.</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="#!" data-toggle="modal" data-target="#orderModal" data-backdrop="static"><i class="fas fa-shopping-cart"></i></a> <a href="https://wa.me/6282330718333" target="_blank"><i class="fab fa-whatsapp"></i></a> <a href="https://t.me/AakZaki" target="_blank"><i class="fab fa-telegram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/exchange.png" alt="Mitra Resmi Indodax">
                    </div>
                    <h4>Deposit Indodax</h4>
                    <div class="line-dec"></div>
                    <p>Ingin menambah deposit ke Indodax? Zaky Cell melayani deposit Indodax dengan menggunakan voucher. Saldo Anda akan langsung bertambah ketika Anda menggunakan voucher yang kami berikan. Proses akan dilakukan setelah kami menerima pembayaran untuk deposit.</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="#!" data-toggle="modal" data-target="#orderModal" data-backdrop="static"><i class="fas fa-shopping-cart"></i></a> <a href="https://wa.me/6282330718333" target="_blank"><i class="fab fa-whatsapp"></i></a> <a href="https://t.me/AakZaki" target="_blank"><i class="fab fa-telegram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/escrow.png" alt="">
                    </div>
                    <h4>Escrow</h4>
                    <div class="line-dec"></div>
                    <p>Anda ingin bertransaksi dengan orang yang tidak dikenal, tapi takut terkena tipu? Zaky Cell memfasilitasi Anda dengan layanan rekber/escrow. Pembeli dapat mentransfer dana kepada kami dan selanjutnya kami akan mengkonfirmasi kepada penjual untuk melakukan pengiriman. Uang Anda 100% aman!</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="#!" data-toggle="modal" data-target="#orderModal" data-backdrop="static"><i class="fas fa-shopping-cart"></i></a> <a href="https://wa.me/6282330718333" target="_blank"><i class="fab fa-whatsapp"></i></a> <a href="https://t.me/AakZaki" target="_blank"><i class="fab fa-telegram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/p2p.png" alt="Binance P2P Indonesia">
                    </div>
                    <h4>Binance P2P IDR</h4>
                    <div class="line-dec"></div>
                    <p>April 2020, Zaky Cell menjadi mitra resmi (P2P Merchant) yang telah terverifikasi untuk layanan pembelian dan penjualan Bitcoin, Ethereum dan USDT dengan menggunakan mata uang Rupiah di Binance exchange yang merupakan exchange nomor #1 dunia. Kini transaksi di Binance jadi lebih mudah!</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="https://p2p.binance.com/id/marchantDetail/097b8c1d641044c984220ef9b946db76" target="_blank">PESAN</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

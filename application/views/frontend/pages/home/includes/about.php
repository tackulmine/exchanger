<section id="about" class="page-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/money.png" alt="">
                    </div>
                    <h4>Withdraw Indodax</h4>
                    <div class="line-dec"></div>
                    <p>Zaky Cell memberikan kemudahan kepada Anda yang ingin menarik uang dari Indodax ke rekening bank Anda dengan cepat. Proses akan dilakukan setelah Anda mengirimkan kode voucher. Waktu rata-rata yang diperlukan untuk transfer ke rekening Anda adalah 1 menit setelah voucher Anda kami gunakan.</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="#!" data-toggle="modal" data-target="#orderModal" data-backdrop="static">PESAN</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/exchange.png" alt="">
                    </div>
                    <h4>Deposit Indodax</h4>
                    <div class="line-dec"></div>
                    <p>Ingin menambah deposit ke Indodax? Zaky Cell melayani deposit Indodax dengan menggunakan voucher. Saldo Anda akan langsung bertambah ketika Anda menggunakan voucher yang kami berikan. Proses akan dilakukan setelah kami menerima pembayaran untuk deposit.</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="#!" data-toggle="modal" data-target="#orderModal" data-backdrop="static">PESAN</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/escrow.png" alt="">
                    </div>
                    <h4>Escrow</h4>
                    <div class="line-dec"></div>
                    <p>Anda ingin bertransaksi dengan orang yang tidak dikenal, tapi takut terkena tipu? Zaky Cell memfasilitasi Anda dengan layanan rekber/escrow. Pembeli dapat mentransfer dana kepada kami dan selanjutnya kami akan mengkonfirmasi kepada penjual untuk melakukan pengiriman. Uang Anda 100% aman!</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="#!" data-toggle="modal" data-target="#orderModal" data-backdrop="static">PESAN</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <img src="/assets/img/phone.png" alt="">
                    </div>
                    <h4>Pulsa Smartphone</h4>
                    <div class="line-dec"></div>
                    <p>Kehabisan pulsa tidak memiliki uang tunai, tetapi memiliki aset Rupiah di Indodax? Kami menerima pengisian pulsa dengan menggunakan voucher Indodax. Dengan harga pulsa yang sesuai counter-counter terdekat. Untuk saat ini, kami tidak melayani pesanan pulsa dengan Crypto</p>
                    <div class="primary-blue-button">
                        <!-- <a href="#" class="scroll-link" data-id="portfolio">Pesan</a> -->
                        <a href="#!" data-toggle="modal" data-target="#orderModal" data-backdrop="static">PESAN</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

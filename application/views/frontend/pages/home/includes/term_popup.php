<div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="termModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="termModalLabel">SYARAT DAN KETENTUAN</h4>
            </div>
            <div class="modal-body">
                <p>Syarat dan ketentuan layanan dan privacy policy Zaky Cell</p>
                <ol>
                    <li>Zaky Cell sebagai pemberi layanan berhak untuk menolak setiap transaksi dengan pelanggan.</li>
                    <li>Zaky Cell tidak menerima pembayaran dengan Bitcoin dalam bentuk apapun.</li>
                    <li>Segala bentuk pembayaran dengan pelanggan dalam bentuk Rupiah (IDR/Rp).</li>
                    <li>Voucher Indodax adalah kode unik yang dihasilkan dari website Indodax.com yang dapat digunakan untuk keperluan Deposit saldo pelanggan ke Indodax atau digunakan untuk melakukan penarikan uang tunai dari Indodax ke rekening pelanggan.</li>
                    <li>Voucher tidak boleh dibagikan kepada orang lain atau bersifat rahasia antara penjual dengan pelanggan.</li>
                    <li>Akun Indodax kami adalah AakZaki, harap diperhatikan dengan seksama, waspada terhadap akun yang menyerupai/serupa.</li>
                    <li>Kami tidak akan melakukan giveaway, kuis, undian berhadiah atau apapun yang bersifat pemberian hadiah di luar website, Bitcointalk maupun social media yang sudah kami sebutkan pada bagian kontak.</li>
                    <li>Berhati-hatilah dengan oknum yang mengatas namakan Zaky Cell, silakan konfirmasi kembali dengan cara kontak kami.</li>
                    <li>Alamat Bitcoin Zaky Cell adalah <b>1ZAKiBoZ6uq3rRBuzhCZMNVfoCTau4SPS</b>.</li>
                    <li>Setiap transaksi yang dilakukan dengan oknum tidak dikenal yang mengatasnamakan Zaky Cell adalah tanggung jawab pelanggan sendiri.</li>
                    <li>Kami tidak menerima komplain atas kesalahan data, alamat email, kode voucher, akun Indodax terkena banned dan lain sebagainya yang sifatnya merupakan kesalahan dari pelanggan itu sendiri.</li>
                    <li>Zaky Cell adalah pihak ke-3 yang menjembatani proses deposit/penarikan yang menjadi mitra resmi dari Indodax.</li>
                    <li>Kami hanya menerima deposit untuk Paypal, Skrill, Payeer dan PerfectMoney dan tidak menerima jasa WD untuk semua layanan yang telah disebutkan.</li>
                    <li>Segala data email merupakan data yang sudah dibagikan secara sadar oleh pelanggan dan akan kami simpan untuk kepentingan internal Zaky Cell.</li>
                    <li>Zaky Cell bertanggung jawab penuh terhadap privasi data-data pelanggan yang diberikan.</li>
                    <li>Biaya escrow, biaya withdraw, biaya layanan PayPal dan PerfectMoney adalah biaya yang harus dibayar sepenuhnya oleh pelanggan.</li>
                    <li>Pengiriman uang kepada pelanggan untuk withdraw dari voucher Indodax akan dilakukan setelah Zaky Cell sukses mengklaim voucher dari pelanggan.</li>
                    <li>Jika, voucher yang diberikan oleh pelanggan ditemukan kadaluarsa, maka Zaky Cell berhak untuk menolak.</li>
                    <li>Per tanggal 14 Januari 2020, Zaky Cell memutuskan untuk <b>menghapus layanan Pulsa Transfer</b>.</li>
                    <li>Zaky Cell berhak untuk memberikan data pelanggan kepada pihak kepolisian atau pihak berwajib untuk keperluan penyidikan jika ditemukan ada pelanggan yang melakukan pelanggaran hukum di wilayah Indonesia.</li>
                    <li>Harap diperhatikan, Domain resmi dari Zaky Cell adalah <b>www.zaky-cell.com</b> dan <b>www.zaky-exchanger.com</b>, apabila Anda melakukan transaksi di luar domain tersebut, maka kami tidak bertanggung jawab atas kehilangan materi dalam bentuk apapun.</li>
                    <li>Jika Anda memiliki pertanyaan, silakan email kami ke cs@zaky-cell.com</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

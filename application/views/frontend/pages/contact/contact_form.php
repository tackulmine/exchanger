<div class="blank" style="padding-top: 100px;padding-bottom: 100px; background: #596065;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">

                            <?php if ($this->session->flashdata('success')): ?>
                                Terima kasih telah menghubungi kami!
                            <?php else: ?>
                                Hubungi Kami
                            <?php endif; ?>
                        </h4>
                    </div>
                    <div class="panel-body">

                        <?php if ($this->session->flashdata('success')): ?>

                            <?php echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div><div class="text-right"><a href="' . site_url() . '" class="btn btn-sm btn-default"><i class="fa fa-home"></i> Home</a></div>'; ?>

                        <?php else: ?>

                            <?php
                            if (validation_errors()) {
                                echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
                            } ?>
                            <?php
                            if (!empty($error)) {
                                echo '<div class="alert alert-danger">' . $error . '</div>';
                            } ?>

                            <?php echo form_open('contact', ['data-toggle' => 'validator']); ?>
                                <input type="hidden" name="contact_recaptcha" id="contact_recaptcha">
                                <div class="form-group">
                                    <label for="contact_name">Nama <strong class="text-danger">*</strong></label>
                                    <input type="text" class="form-control" id="contact_name" name="contact_name" value="<?php echo set_value('contact_name') ?>" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="contact_email">Email <strong class="text-danger">*</strong></label>
                                    <input type="email" class="form-control" id="contact_email" name="contact_email" value="<?php echo set_value('contact_email') ?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="contact_message">Pesan<strong class="text-danger">*</strong></label>
                                    <textarea name="contact_message" id="contact_message" class="form-control" rows="3" required="required"><?php echo set_value('contact_message') ?></textarea>
                                </div>
                                <button type="submit" class="btn btn-default">Kirim</button>
                            <?php echo form_close(); ?>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- on form -->
<form method="POST" action="<?=site_url('register/index')?>" class="form-horizontal" role="form">
    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
</form>

<!-- on ajax -->
var csfrData = {};
csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
$.ajaxSetup({
    data: csfrData
});

$.ajax({
    url: '<?php echo site_url("post/update") ?>',
    type: 'POST',
    dataType: 'json',
    data: {
        data: 'data',
        <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'
    },
});
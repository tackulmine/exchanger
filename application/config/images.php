<?php
/*
|--------------------------------------------------------------------------
| Image Preset Sizes
|--------------------------------------------------------------------------
|
| Specify the preset sizes you want to use in your code. Only these preset
| will be accepted by the controller for security.
|
| Each preset exists of a width and height. If one of the dimensions are
| equal to 0, it will automatically calculate a matching width or height
| to maintain the original ratio.
|
| If both dimensions are specified it will automatically crop the
| resulting image so that it fits those dimensions.
|
*/

$config["image_sizes"]["xsmall_square"] = array(100, 100);
$config["image_sizes"]["small_square"]  = array(300, 300);
$config["image_sizes"]["medium_square"] = array(500, 500);
$config["image_sizes"]["large_square"] = array(800, 800);

$config["image_sizes"]["wide"]   = array(800, 420);

// $config["image_sizes"]["long"]   = array(280, 600);
// $config["image_sizes"]["wide"]   = array(600, 200);
// $config["image_sizes"]["hero"]   = array(940, 320);

$config["image_sizes"]["xsmall"] = array(100, 0);
$config["image_sizes"]["small"]  = array(300, 0);
$config["image_sizes"]["medium"] = array(500, 0);
$config["image_sizes"]["large"]  = array(800, 0);
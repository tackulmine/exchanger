<?php defined('BASEPATH') or exit('No direct script access allowed');

class Post_seeder extends Seeder
{
    private $table = 'posts';
    private $pivot_tag_table = 'post_tag_pivot';

    public function run()
    {
        $this->load->helper(['url', 'common', 'db']);
        $this->load->model('category_model');

        $this->db->query(disableForeignKeyChecks());
        $this->db->empty_table($this->pivot_tag_table);
        $this->db->empty_table($this->table);
        $this->db->truncate($this->pivot_tag_table);
        $this->db->truncate($this->table);
        $this->db->query(enableForeignKeyChecks());

        // //seed records manually
        // $data = ['user_name' => 'admin', 'password' => '9871'];
        // $this->db->insert($this->table, $data);

        //seed many records using faker
        $limit = 13;
        echo "seeding $limit records";
        $tmp_images = [];
        $image_path = "uploads/posts/";
        if (!is_dir(FCPATH . $image_path)) {
            mkdir(FCPATH . $image_path, 0777, true);
        } else {
            $this->load->helper('file');
            // delete all uploaded files for the company
            delete_files(FCPATH . $image_path, TRUE);
        }
        for ($i = 0; $i < $limit; $i++) {
            echo ".";

            $category_id = Category_model::orderByRaw("RAND()")->first()->id;
            $title = $this->faker->unique()->sentence;
            $slug = url_title($title, '-', true);
            $image = $this->faker->image($image_path, 800, 600, null, false);
            $tmp_images[] = $image;
            $content_html = "<p>" . implode("</p><p>", $this->faker->paragraphs(rand(1,3))) . "</p>";
            $content_raw = strip_tags($content_html);

            $data = [
                'category_id' => $category_id,
                'slug' => $slug,
                'title' => $title,
                'subtitle' => $this->faker->sentence,
                'content_raw' => $content_raw,
                'content_html' => $content_html,
                'image_path' => $image_path,
                'image' => $image,
                'meta_title' => $this->faker->sentence,
                'meta_description' => $this->faker->sentence,
                'is_draft' => /*rand(0, 1)*/0,
                'published_at' => Carbon\Carbon::now()->subMinutes(($i*5)),
                'created_by' => 1,
            ];
            $this->db->insert($this->table, $data);
            $post_id = $this->db->insert_id();

            $tag_ids = Tag_model::orderByRaw("RAND()")->limit(rand(1,3))->where('is_draft', 0)->pluck('id');
            $pivot_data = [];
            foreach ($tag_ids as $tag_id) {
                $pivot_data[] = ['post_id' => $post_id, 'tag_id' => $tag_id];
            }
            if (count($pivot_data)) {
                $this->db->insert_batch($this->pivot_tag_table, $pivot_data);
            }

        }

        // foreach ($tmp_images as $image) {
        //     resizeImage($image, $image_path);
        // }

        echo PHP_EOL;
    }
}

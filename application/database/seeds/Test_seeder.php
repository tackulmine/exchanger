<?php defined('BASEPATH') or exit('No direct script access allowed');

class Test_seeder extends Seeder
{
    private $table = 'tests';

    public function run()
    {
        $this->load->helper('url');

        // // === optionals
        // $this->load->helper('common');
        // // === optionals

        // === optionals
        $this->load->helper('db');
        $this->db->query(disableForeignKeyChecks());
        $this->db->empty_table($this->table);
        $this->db->truncate($this->table);
        $this->db->query(enableForeignKeyChecks());
        // === optionals

        // // === optionals
        // //seed records manually
        // $data = ['name' => 'name'];
        // $this->db->insert($this->table, $data);
        // // === optionals

        //seed many records using faker
        $limit = 10;
        echo 'seeding {$limit} records';

        // // === optionals
        // $tmp_images = [];
        // // === optionals

        for ($i = 0; $i < $limit; $i++) {
            echo '.';

            $title = $this->faker->unique()->word;
            $slug = url_title($title, '-', true);

            // // === optionals
            // $image_path = 'uploads/';
            // $image = $this->faker->image($image_path, 800, 600, null, false);
            // $tmp_images[] = $image;
            // // === optionals

            $data = [
                'slug' => $slug,
                'title' => $title,

                // // === optionals
                // 'image_path' => $image_path,
                // 'image' => $image,
                // // === optionals

            ];
            $this->db->insert($this->table, $data);
        }

        // // === optionals
        // foreach ($tmp_images as $image) {
        //     resizeImage($image, $image_path);
        // }
        // // === optionals

        echo PHP_EOL;
    }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Tag_seeder extends Seeder
{
    private $table = 'tags';

    public function run()
    {
        $this->load->helper(['url', 'common', 'db']);

        $this->db->query(disableForeignKeyChecks());
        $this->db->empty_table($this->table);
        $this->db->truncate($this->table);
        $this->db->query(enableForeignKeyChecks());

        // //seed records manually
        // $data = ['user_name' => 'admin', 'password' => '9871'];
        // $this->db->insert($this->table, $data);

        //seed many records using faker
        $limit = 13;
        echo "seeding $limit records";
        $tmp_images = [];
        $image_path = "uploads/tags/";
        if (!is_dir(FCPATH . $image_path)) {
            mkdir(FCPATH . $image_path, 0777, true);
        } else {
            $this->load->helper('file');
            // delete all uploaded files for the company
            delete_files(FCPATH . $image_path, TRUE);
        }
        for ($i = 0; $i < $limit; $i++) {
            echo ".";

            $title = $this->faker->unique()->word;
            $slug = url_title($title, '-', true);
            $image = $this->faker->image($image_path, 800, 600, null, false);
            $tmp_images[] = $image;
            $data = [
                // 'id' => $id,
                'slug' => $slug,
                'title' => $title,
                'subtitle' => $this->faker->sentence,
                'description' => $this->faker->paragraph,
                'image_path' => $image_path,
                'image' => $image,
                'meta_title' => $this->faker->sentence,
                'meta_description' => $this->faker->sentence,
                'is_draft' => rand(0, 1),
                'created_by' => 1,
            ];
            $this->db->insert($this->table, $data);
        }

        // foreach ($tmp_images as $image) {
        //     resizeImage($image, $image_path);
        // }

        echo PHP_EOL;
    }
}

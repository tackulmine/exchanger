<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_table_post_tag_pivot extends CI_Migration
{
    private $table;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'post_tag_pivot';
        $this->load->helper('db');
    }

    public function up()
    {
        $this->dbforge->add_field([
            // 'id' => [
            //     'type' => 'INT',
            //     'constraint' => 10,
            //     'unsigned' => true,
            //     'auto_increment' => true,
            // ],
            'post_id' => [
                'type' => 'INT',
                'constraint' => '10',
                'unsigned' => true,
            ],
            'tag_id' => [
                'type' => 'INT',
                'constraint' => '10',
                'unsigned' => true,
            ],
        ]);
        $this->dbforge->add_key('post_id', true);
        $this->dbforge->add_key('tag_id', true);
        // $this->dbforge->add_key('post_id', 'tag_id');
        $this->dbforge->create_table($this->table);

        $this->db->query(add_foreign_key($this->table, 'post_id', 'posts(id)', 'CASCADE', 'CASCADE'));
        $this->db->query(add_foreign_key($this->table, 'tag_id', 'tags(id)', 'CASCADE', 'CASCADE'));
    }

    public function down()
    {
        $this->db->query(drop_foreign_key($this->table, 'tag_id'));
        $this->db->query(drop_foreign_key($this->table, 'post_id'));
        $this->dbforge->drop_table($this->table);
    }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_table_posts extends CI_Migration
{
    private $table;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'posts';
        $this->load->helper('db');
    }

    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'category_id' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => true,
            ],
            'slug' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => true,
                'unique' => true,
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'subtitle' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'content_raw' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'content_html' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'image_path' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => true,
            ],
            'image' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => true,
            ],
            'meta_title' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
            ],
            'meta_description' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true,
            ],
            'is_draft' => [
                'type' => 'TINYINT',
                'constraint' => '1',
                'default' => 0,
            ],
            'published_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'visited_count' => [
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => true,
                'default' => 0,
            ],
            // 'created_at' => [
            //     'type' => 'TIMESTAMP',
            // ],
            // 'updated_at' => [
            //     'type' => 'TIMESTAMP',
            //     'null' => true,
            // ],
            // 'created_by' => [
            //     'type' => 'INT',
            //     'constraint' => '10',
            //     'unsigned' => true,
            // ],
            // 'updated_by' => [
            //     'type' => 'INT',
            //     'constraint' => '10',
            //     'unsigned' => true,
            //     'null' => true,
            // ],
        ]);
        $this->dbforge->add_field("`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_field("`updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP");
        $this->dbforge->add_field([
            'created_by' => [
                'type' => 'INT',
                'constraint' => '10',
                'unsigned' => true,
            ],
            'updated_by' => [
                'type' => 'INT',
                'constraint' => '10',
                'unsigned' => true,
                'null' => true,
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_key('category_id');
        $this->dbforge->add_key('is_draft');
        $this->dbforge->add_key('created_by');
        $this->dbforge->add_key('updated_by');
        $this->dbforge->create_table($this->table);

        $this->db->query(add_foreign_key($this->table, 'created_by', 'users(id)', 'CASCADE', 'CASCADE'));
        $this->db->query(add_foreign_key($this->table, 'updated_by', 'users(id)', 'CASCADE', 'CASCADE'));
        $this->db->query(add_foreign_key($this->table, 'category_id', 'categories(id)', 'CASCADE', 'CASCADE'));
    }

    public function down()
    {
        $this->db->query(drop_foreign_key($this->table, 'category_id'));
        $this->db->query(drop_foreign_key($this->table, 'updated_by'));
        $this->db->query(drop_foreign_key($this->table, 'created_by'));
        $this->dbforge->drop_table($this->table);
    }
}

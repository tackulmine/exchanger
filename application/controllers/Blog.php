<?php defined('BASEPATH') or exit('No direct script access allowed');

class Blog extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('post_model');

        $zaky_sidebar_posts = Post_model::with([
            'category' => function ($q) {
                $q->published();
            }])
            ->whereHas('category', function ($q) {
                $q->published();
            })
            ->published()
            ->limit(4)
            ->orderBy('published_at', 'desc')
            ->get();
        $zaky_sidebar_categories = Category_model::withCount([
            'posts' => function ($q) {
                $q->published();
            }])
            ->whereHas('posts', function ($q) {
                $q->published();
            })
            ->published()
            ->limit(5)
            ->orderByRaw("RAND()")
            ->get();
        $zaky_sidebar_tags = Tag_model::with([
            'posts' => function ($q) {
                $q->published();
            }])
            ->whereHas('posts', function ($q) {
                $q->published();
            })
            ->published()
            ->limit(5)
            ->orderByRaw("RAND()")
            ->get();

        $data = [
            'sidebar_posts' => $zaky_sidebar_posts,
            'sidebar_categories' => $zaky_sidebar_categories,
            'sidebar_tags' => $zaky_sidebar_tags,
        ];

        $this->global_data = array_merge($this->global_data, $data);
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->page();
    }

    public function page(int $page = 0)
    {
        $per_page = 6;
        $posts = Post_model::with([
            'category' => function ($q) {
                $q->published();
            },
            'tags' => function ($q) {
                $q->published();
            }])
            ->whereHas('category', function ($q) {
                $q->published();
            })
            ->published();
        if ($this->input->get('search')) {
            $posts = $posts->where('title', 'like', $this->input->get('search') . '%');
        }
        $zaky_total_posts = $posts->count();
        $zaky_posts = $posts->skip($page)
            ->take($per_page)
            ->orderBy('published_at', 'desc')
            ->get();
        $pagination = $this->set_pagination('blog', $zaky_total_posts, $per_page, 2, 3);

        $data = [
            'page_title' => 'Blog ' . APP_NAME,
            'subtitle' => 'Tempat berbagi informasi seputar crypto dan indodax.',
            'meta_title' => 'Blog ' . APP_NAME,
            'meta_description' => 'Blog ' . APP_NAME . ' sebagai tempat berbagi informasi seputar crypto dan indodax.',
            'posts' => $zaky_posts,
            'pagination' => $pagination,
        ];

        if ($this->input->get('search')) {
            $data = array_merge($data, [
                'page_title' => "Hasil pencarian dengan kata kunci '{$this->input->get('search')}'",
                'meta_title' => "Hasil pencarian artikel dengan kata kunci '{$this->input->get('search')}' di Blog " . APP_NAME,
            ]);
        }

        $this->_render_template($this->web_dir . 'pages/blog/index', $data);
    }

    public function category(string $category_slug, int $page = 0)
    {
        $category = Category_model::where('slug', $category_slug)
            ->published()
            ->first();

        if (empty($category)) {
            show_404();
        }

        // increment visited count
        $category->increment('visited_count');

        $per_page = 6;
        $posts = Post_model::with([
            'category' => function ($q) {
                $q->published();
            }])
            ->whereHas('category', function ($q) use ($category) {
                $q->where('id', $category->id)
                    ->published();
            })
            ->published();
        $zaky_total_posts = $posts->count();
        // var_dump($zaky_total_posts); die;
        $zaky_posts = $posts->skip($page)
            ->take($per_page)
            ->orderBy('published_at', 'asc')
            ->get();
        $pagination = $this->set_pagination('kategori/' . $category_slug, $zaky_total_posts, $per_page, 3, 3);

        $data = [
            'page_title' => 'Kategori ' . $category->title,
            'subtitle' => $category->subtitle,
            'meta_title' => $category->meta_title,
            'meta_description' => $category->meta_description,
            'category' => $category,
            'posts' => $zaky_posts,
            'pagination' => $pagination,
        ];

        $this->_render_template($this->web_dir . 'pages/blog/index', $data);
    }

    public function tag(string $tag_slug, int $page = 0)
    {
        $tag = Tag_model::where('slug', $tag_slug)
            ->published()
            ->first();

        if (empty($tag)) {
            show_404();
        }

        // increment visited count
        $tag->increment('visited_count');

        $per_page = 6;
        $posts = Post_model::with([
            'tags' => function ($q) {
                $q->published();
            }])
            ->whereHas('tags', function ($q) use ($tag) {
                $q->where('id', $tag->id)
                    ->published();
            })
            ->published();
        $zaky_total_posts = $posts->count();
        // var_dump($zaky_total_posts); die;
        $zaky_posts = $posts->skip($page)
            ->take($per_page)
            ->orderBy('published_at', 'asc')
            ->get();
        $pagination = $this->set_pagination('tag/' . $tag_slug, $zaky_total_posts, $per_page, 3, 3);

        $data = [
            'page_title' => 'Topik ' . $tag->title,
            'subtitle' => $tag->subtitle,
            'meta_title' => $tag->meta_title,
            'meta_description' => $tag->meta_description,
            'tag' => $tag,
            'posts' => $zaky_posts,
            'pagination' => $pagination,
        ];

        $this->_render_template($this->web_dir . 'pages/blog/index', $data);
    }

    public function detail(string $post_slug)
    {
        $post = Post_model::with('category')
            ->withCount([
                'tags' => function ($q) {
                    $q->published();
                }])
            ->whereHas('category', function ($q) {
                $q->published();
            })
            ->where('slug', $post_slug)
            ->published()
            ->first();

        if (empty($post)) {
            show_404();
        }

        // increment visited count
        $post->increment('visited_count');

        $data = [
            'page_title' => $post->title,
            'subtitle' => $post->subtitle,
            'meta_title' => $post->meta_title,
            'meta_description' => $post->meta_description,
            'post' => $post,
            'newer_post' => $post->newerPost(),
            'older_post' => $post->olderPost(),
        ];

        $this->_render_template($this->web_dir . 'pages/blog/detail', $data);
    }
}

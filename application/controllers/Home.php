<?php defined('BASEPATH') or exit('No direct script access allowed');

class Home extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('user_model');
        $this->load->library('curl');
        $this->load->helper('indodax');
        $this->load->driver('cache');
    }

    protected function _getIndodaxTickers()
    {
        // $indodaxUrl = 'https://indodax.com/api/tickers';
        $indodaxUrl = 'https://api.coingecko.com/api/v3/exchanges/Indodax/tickers';

        // return btcid_query('tickers');

        # use simple curl
        return $this->curl->simple_get($indodaxUrl);

        // // # use guzzle
        // $client = new \GuzzleHttp\Client();
        // try {
        //     # guzzle get request example
        //     $response = $client->request('GET', $indodaxUrl);

        //     # guzzle post request example with form parameter
        //     // $response = $client->request('POST', $url, [
        //     //         'form_params'=> [
        //     //             'processId' => '2'
        //     //         ],
        //     //     ]
        //     // );

        //     # guzzle repose for future use
        //     // echo $response->getStatusCode(); // 200
        //     // echo $response->getReasonPhrase(); // OK
        //     // echo $response->getProtocolVersion(); // 1.1
        //     // echo $response->getBody();

        //     return $response->getBody();
        // } catch (GuzzleHttp\Exception\BadResponseException $e) {
        //     #guzzle repose for future use
        //     $response = $e->getResponse();
        //     $responseBodyAsString = $response->getBody()->getContents();
        //     // print_r($responseBodyAsString);
        //     if (ENVIRONMENT == "production") {
        //         log_message('error', 'BadResponseException: ' . $responseBodyAsString);
        //         return false;
        //     } else {
        //         throw new Exception('BadResponseException: ' . $responseBodyAsString);
        //     }
        // }
    }

    protected function _getCoinMarkets()
    {
        $coinMarketUrl = 'https://api.coingecko.com/api/v3/coins/markets';
        $params = [
            'vs_currency' => 'idr',
            'price_change_percentage' => '1h,24h,7d',
            // 'order' => 'total_volume_desc',
            'order' => 'market_cap_desc',
        ];

        # use simple curl
        return $this->curl->simple_get($coinMarketUrl, $params);

        // // # use guzzle
        // $client = new \GuzzleHttp\Client();
        // try {
        //     # guzzle get request example
        //     $response = $client->request('GET', $coinMarketUrl, [
        //         'query' => $params
        //     ]);

        //     # guzzle post request example with form parameter
        //     // $response = $client->request('POST', $url, [
        //     //         'form_params'=> [
        //     //             'processId' => '2'
        //     //         ],
        //     //     ]
        //     // );
        //     #guzzle repose for future use
        //     // echo $response->getStatusCode(); // 200
        //     // echo $response->getReasonPhrase(); // OK
        //     // echo $response->getProtocolVersion(); // 1.1
        //     // echo $response->getBody();

        //     return $response->getBody();
        // } catch (GuzzleHttp\Exception\BadResponseException $e) {
        //     #guzzle repose for future use
        //     $response = $e->getResponse();
        //     $responseBodyAsString = $response->getBody()->getContents();
        //     // print_r($responseBodyAsString);
        //     if (ENVIRONMENT == "production") {
        //         log_message('error', 'BadResponseException: ' . $responseBodyAsString);
        //         return false;
        //     } else {
        //         throw new Exception('BadResponseException: ' . $responseBodyAsString);
        //     }
        // }
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        if (USING_CACHE) {
            if (!$indodaxTickers = $this->cache->file->get('indodax_tickers')) {
                $indodaxTickers = $this->_getIndodaxTickers();
                $this->cache->file->save('indodax_tickers', $indodaxTickers, CACHE_TIMEOUT);
            }
        } else {
            $indodaxTickers = $this->_getIndodaxTickers();
        }

        if (!$indodaxTickers = json_decode($indodaxTickers, true)) {
            if (ENVIRONMENT == "production") {
                log_message('error', 'Invalid data received, please make sure connection is working and requested API exists: ' . $indodaxTickers);
            } else {
                throw new Exception('Invalid data received, please make sure connection is working and requested API exists: ' . $indodaxTickers);
            }
        }

        $indodaxCollections = new Illuminate\Support\Collection($indodaxTickers['tickers']);
        $newCollections = new Illuminate\Support\Collection();
        foreach ($indodaxCollections as $collection) {
            $collection['convert'] = strtolower($collection['base'] . '_' . $collection['target']);
            $newCollections[] = $collection;
        }
        $indodaxCollections = $newCollections->keyBy('convert');

        $zakyFilterTickers = [
            'btc_idr' => "Bitcoin",
            'waves_idr' => "Waves",
            'doge_idr' => "Doge",
            'ont_idr' => "Ontology",
            // 'bchsv_idr' => "BCHSV",
            // 'bchabc_idr' => "BCHABC",
            'eth_idr' => "Ethereum",
            'ltc_idr' => "Litecoin",
            'xrp_idr' => "Ripple",
            'str_idr' => "Stellar",
            'etc_idr' => "Ethereum Classic",
            'usdt_idr' => "Paypal",
        ];
        $indodaxTickers = convert_indodax_tickers($indodaxCollections->toArray(), $zakyFilterTickers);

        if (USING_CACHE) {
            if (!$coingeckoCoinMarkets = $this->cache->file->get('coingecko_coin_markets')) {
                $coingeckoCoinMarkets = $this->_getCoinMarkets();
                $this->cache->file->save('coingecko_coin_markets', $coingeckoCoinMarkets, CACHE_TIMEOUT);
            }
        } else {
            $coingeckoCoinMarkets = $this->_getCoinMarkets();
        }

        if (!$coingeckoCoinMarkets = json_decode($coingeckoCoinMarkets, true)) {
            if (ENVIRONMENT == "production") {
                log_message('error', 'Invalid data received, please make sure connection is working and requested API exists: ' . $coingeckoCoinMarkets);
            } else {
                throw new Exception('Invalid data received, please make sure connection is working and requested API exists: ' . $coingeckoCoinMarkets);
            }
        }

        $coingeckoCoinMarkets = setKeyBy('symbol', $coingeckoCoinMarkets);
        $zaky_filter_markets = [
            'btc',
            'eth',
            'ltc',
            'xrp',
            'xlm',
            'etc',
            'usdt',
            'eos',
            'bnb',
            'ada',
            'xtz',
            'trx',
            'atom',
            'leo',
            'okb',
            'ht',
            'link',
            'neo',
            'usdc',
            'miota',
            'cro',
            'dash',
        ];
        $zaky_coingecko_coin_markets = convert_coingecko_markets($coingeckoCoinMarkets, $zaky_filter_markets);

        // get 4 blog posts terbaru
        $this->load->model('post_model');
        $zaky_posts = Post_model::with('category')
            ->whereHas('category', function ($q) {
                $q->published();
            })
            ->published()
            ->hasCover()
            ->limit(4)
            ->orderBy('published_at', 'desc')
            ->get();

        $data = [
            'page_title' => 'Home',
            'indodax_tickers' => $indodaxTickers,
            'coingecko_coin_markets' => $coingeckoCoinMarkets,
            'zaky_coingecko_coin_markets' => $zaky_coingecko_coin_markets,
            'posts' => $zaky_posts,
            // 'fmt' => new NumberFormatter('en_ID', NumberFormatter::CURRENCY)
        ];

        $this->_render_template($this->web_dir . 'pages/home/index', $data);
    }
}

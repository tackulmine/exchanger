<?php defined('BASEPATH') or exit('No direct script access allowed');

class Home extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('user_model');
        $this->load->library('curl');
        $this->load->helper('indodax');
        $this->load->driver('cache');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        // $users = User_model::all()->toArray();
        // print_r('<pre>');
        // var_dump($users); //die;

        if (!$indodax_tickers = $this->cache->file->get('indodax_tickers')) {
            $indodax_tickers = btcid_query('tickers');
            // 60 seconds = 1 menit
            $this->cache->file->save('indodax_tickers', $indodax_tickers, 60);
        }
        $zaky_filter_tickers = [
            'btc_idr' => "Bitcoin",
            'bchsv_idr' => "BCHSV",
            'bchabc_idr' => "BCHABC",
            'eth_idr' => "Ethereum",
            'ltc_idr' => "Litecoin",
            'xrp_idr' => "Ripple",
            'str_idr' => "Stellar",
            'etc_idr' => "Ethereum Classic",
            'usdt_idr' => "Paypal",
        ];
        $indodax_tickers = convert_indodax_tickers($indodax_tickers['tickers'], $zaky_filter_tickers);
        // var_dump($indodax_tickers); die;

        if (!$coingecko_coin_markets = $this->cache->file->get('coingecko_coin_markets')) {
            $coingecko_coin_markets = $this->curl->simple_get('https://api.coingecko.com/api/v3/coins/markets', [
                'vs_currency' => 'idr',
                'price_change_percentage' => '1h,24h,7d',
                // 'order' => 'total_volume_desc',
                'order' => 'market_cap_desc',
            ]);
            // 60 seconds = 1 menit
            $this->cache->file->save('coingecko_coin_markets', $coingecko_coin_markets, 60);
        }
        // print_r(json_decode($coingecko_coin_markets)); die;
        $coingecko_coin_markets = json_decode($coingecko_coin_markets, true);
        // print_r($coingecko_coin_markets); die;
        $coingecko_coin_markets = setKeyBy('symbol', $coingecko_coin_markets);
        // var_dump($coingecko_coin_markets); die;
        $zaky_filter_markets = [
            'btc',
            'eth',
            'ltc',
            'xrp',
            'xlm',
            'etc',
            'usdt',
            'eos',
            'bnb',
            'ada',
            'xtz',
            'trx',
            'atom',
            'leo',
            'okb',
            'ht',
            'link',
            'neo',
            'usdc',
            'miota',
            'cro',
            'dash',
        ];
        $zaky_coingecko_coin_markets = convert_coingecko_markets($coingecko_coin_markets, $zaky_filter_markets);
        // var_dump($zaky_coingecko_coin_markets); die;

        // get 4 blog posts terbaru
        $this->load->model('post_model');
        $zaky_posts = Post_model::with('category')
            ->whereHas('category', function ($q) {
                $q->published();
            })
            ->published()
            ->hasCover()
            ->limit(4)
            ->orderBy('published_at', 'desc')
            ->get();

        $data = [
            'page_title' => 'Home',
            'indodax_tickers' => $indodax_tickers,
            'coingecko_coin_markets' => $coingecko_coin_markets,
            'zaky_coingecko_coin_markets' => $zaky_coingecko_coin_markets,
            'posts' => $zaky_posts,
            // 'fmt' => new NumberFormatter('en_ID', NumberFormatter::CURRENCY)
        ];

        $this->_render_template($this->web_dir . 'pages/home/index', $data);
    }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

@require_once APPPATH . 'database/Seeder.php';

class Tools extends CI_Controller
{
    public $faker;

    public function __construct()
    {
        parent::__construct();
        // can only be called from the command line
        if (!$this->input->is_cli_request()) {
            exit('Direct access is not allowed. This is a command line tool, use the terminal');
        }
        $this->load->dbforge();
        $this->load->helper('inflector');

        // initiate faker
        // $this->faker = \Faker\Factory::create();
        // var_dump($this->faker); die;
    }

    public function message($to = 'World')
    {
        echo "Hello {$to}!" . PHP_EOL;
    }

    public function help()
    {
        $result = "The following are the available command line interface commands\n\n";
        $result .= "php index.php tools migration \"file_name\" \"table_name\" Create new migration file\n";
        $result .= "php index.php tools migrate [\"version_number\"] Run all migrations. The version number is optional.\n";
        $result .= "php index.php tools seeder \"file_name\" \"table_name\" Creates a new seed file.\n";
        $result .= "php index.php tools seed \"file_name\" Run the specified seed file.\n";
        $result .= "php index.php tools model \"file_name\" \"table_name\" Creates a new model file.\n";

        echo $result . PHP_EOL;
    }

    public function migration($name, $table = null)
    {
        $this->make_migration_file($name, $table);
    }

    public function migrate($version = null)
    {
        $this->load->library('migration');

        if ($version != null) {
            if ($this->migration->version($version) === false) {
                show_error($this->migration->error_string());
            } else {
                echo "Migrations run successfully" . PHP_EOL;
            }
            return;
        }

        if ($this->migration->latest() === false) {
            show_error($this->migration->error_string());
        } else {
            echo "Migrations run successfully" . PHP_EOL;
        }
    }

    public function seeder($name, $table = null)
    {
        $this->make_seed_file($name, $table);
    }

    public function seed($name)
    {
        $seeder = new Seeder();
        $seeder->call($name);
    }

    protected function make_migration_file($name, $table = null)
    {
        $file_name = strtolower($name);
        $name = ucfirst($name);
        $table_name = !empty($table) ? $table : $name;
        $table_name = strtolower(plural($table_name));

        $date = new DateTime();
        $timestamp = $date->format('YmdHis');
        $path = APPPATH . "database/migrations/{$timestamp}_{$file_name}.php";
        $my_migration = fopen($path, "w") or die("Unable to create migration file!");
        $migration_template = "<?php defined('BASEPATH') or exit('No direct script access allowed');\n
class Migration_{$name} extends CI_Migration
{
    private \$table;

    public function __construct()
    {
        parent::__construct();
        \$this->table = '{$table_name}';

        // // === optionals
        // \$this->load->helper('db');
        // // === optionals
    }

    public function up()
    {
        \$this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => true,
                'auto_increment' => true,
            ],
        ]);

        // // === optionals
        // \$this->dbforge->add_field('`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        // \$this->dbforge->add_field('`updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP');
        // \$this->dbforge->add_field([
        //     'created_by' => [
        //         'type' => 'INT',
        //         'constraint' => '10',
        //         'unsigned' => true,
        //     ],
        //     'updated_by' => [
        //         'type' => 'INT',
        //         'constraint' => '10',
        //         'unsigned' => true,
        //         'null' => true,
        //     ],
        // ]);
        // // === optionals

        \$this->dbforge->add_key('id', true);

        // === optionals
        \$this->dbforge->add_key('created_by');
        \$this->dbforge->add_key('updated_by');
        // === optionals

        \$this->dbforge->create_table(\$this->table);

        // // === optionals
        // \$this->db->query(add_foreign_key(\$this->table, 'created_by', 'users(id)', 'CASCADE', 'CASCADE'));
        // \$this->db->query(add_foreign_key(\$this->table, 'updated_by', 'users(id)', 'CASCADE', 'CASCADE'));
        // // === optionals
    }

    public function down()
    {
        // // === optionals
        // \$this->db->query(drop_foreign_key(\$this->table, 'updated_by'));
        // \$this->db->query(drop_foreign_key(\$this->table, 'created_by'));
        // // === optionals

        \$this->dbforge->drop_table(\$this->table);
    }
}
";
        fwrite($my_migration, $migration_template);
        fclose($my_migration);
        echo "$path migration has successfully been created." . PHP_EOL;
    }

    protected function make_seed_file($name, $table = null)
    {
        $file_name = ucfirst(singular(str_replace(['_seeder', 'seeder', 'Seeder'], '', $name))) . "_seeder";
        $name = $file_name;
        $table_name = !empty($table) ? $table : $name;
        $table_name = strtolower(plural(str_replace(['_seeder', 'seeder', 'Seeder'], '', $table_name)));

        $path = APPPATH . "database/seeds/{$file_name}.php";
        $my_seed = fopen($path, "w") or die("Unable to create seed file!");
        $seed_template = "<?php defined('BASEPATH') or exit('No direct script access allowed');\n
class {$name} extends Seeder
{
    private \$table = '{$table_name}';

    public function run()
    {
        \$this->load->helper('url');

        // // === optionals
        // \$this->load->helper('common');
        // // === optionals

        // === optionals
        \$this->load->helper('db');
        \$this->db->query(disableForeignKeyChecks());
        \$this->db->empty_table(\$this->table);
        \$this->db->truncate(\$this->table);
        \$this->db->query(enableForeignKeyChecks());
        // === optionals

        // // === optionals
        // //seed records manually
        // \$data = ['name' => 'name'];
        // \$this->db->insert(\$this->table, \$data);
        // // === optionals

        //seed many records using faker
        \$limit = 10;
        echo 'seeding {\$limit} records';

        // // === optionals
        // \$tmp_images = [];
        // // === optionals

        for (\$i = 0; \$i < \$limit; \$i++) {
            echo '.';

            \$title = \$this->faker->unique()->word;
            \$slug = url_title(\$title, '-', true);

            // // === optionals
            // \$image_path = 'uploads/';
            // if (!is_dir(FCPATH . \$image_path)) {
            //     mkdir(FCPATH . \$image_path, 0777, true);
            // }
            // \$image = \$this->faker->image(\$image_path, 800, 600, null, false);
            // \$tmp_images[] = \$image;
            // // === optionals

            \$data = [
                'slug' => \$slug,
                'title' => \$title,

                // // === optionals
                // 'image_path' => \$image_path,
                // 'image' => \$image,
                // // === optionals

            ];
            \$this->db->insert(\$this->table, \$data);
        }

        // // === optionals
        // foreach (\$tmp_images as \$image) {
        //     resizeImage(\$image, \$image_path);
        // }
        // // === optionals

        echo PHP_EOL;
    }
}
";
        fwrite($my_seed, $seed_template);
        fclose($my_seed);
        echo "$path seeder has successfully been created." . PHP_EOL;
    }

    public function model($name, $table = null)
    {
        $this->make_model_file($name, $table);
    }

    protected function make_model_file($name, $table = null)
    {
        $file_name = ucfirst(str_replace(['_model', 'model', 'Model'], '', $name)) . "_model";
        $name = $file_name;
        $table_name = !empty($table) ? $table : $name;
        $table_name = strtolower(plural(str_replace(['_model', 'model', 'Model'], '', $table_name)));

        $path = APPPATH . "models/{$file_name}.php";
        $my_model = fopen($path, "w") or die("Unable to create model file!");
        $model_template = "<?php defined('BASEPATH') OR exit('No direct script access allowed');\n
use \Illuminate\Database\Eloquent\Model as Eloquent;

class {$name} extends Eloquent
{
    protected \$table = '{$table_name}';
}
";
        fwrite($my_model, $model_template);
        fclose($my_model);
        echo "$path model has successfully been created." . PHP_EOL;
    }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $data = [
            'page_title' => 'Kontak Kami!',
        ];

        $this->load->helper(['form', 'url']);
        $this->load->library('curl');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('contact_name', 'Nama', 'trim|required|min_length[2]|max_length[100]');
        $this->form_validation->set_rules('contact_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('contact_message', 'Pesan', 'trim|required');

        $this->form_validation->set_error_delimiters('', '<br>');

        if ($this->form_validation->run()) {
            $recaptcha_verify = $this->curl->simple_post('https://www.google.com/recaptcha/api/siteverify', [
                'secret' => RECAPTCHA_SECRET_KEY,
                'response' => $this->input->post('contact_recaptcha'),
            ]);
            // var_dump($recaptcha_verify); die;
            $response = json_decode($recaptcha_verify);
            // var_dump($response->success); die;
            if ($response->success === false) {
                $data['error'] = 'Gagal memverifikasi aktifitas kamu bukanlah robot, silakan mengulangi kembali!';
                // $this->session->flashdata('error', 'You are not a human!');
            } else {
                // send email to admin
                // var_dump($this->input->post()); die;

                $this->load->library('email');
                $config = [];
                if (ENVIRONMENT == "development") {
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = 'smtp.mailtrap.io';
                    $config['smtp_user'] = 'f4341efc76733a';
                    $config['smtp_pass'] = '12657fe3c21f6b';
                    $config['smtp_port'] = '2525';
                }
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";
                // $config['charset'] = 'iso-8859-1';

                $this->email->clear();
                $this->email->initialize($config);
                // $this->email->set_newline("\r\n");
                // $this->email->set_mailtype("html");

                $this->email->from('no-reply@' . $_SERVER['HTTP_HOST'], 'No-Reply ' . APP_NAME);
                $this->email->to(CONTACT_EMAIL);

                if (ENVIRONMENT != "development") {
                    $this->email->cc(ADMIN_EMAIL);
                    $this->email->bcc(DEVELOPER_EMAIL);
                }

                $this->email->subject('Formulir Kontak Baru @ ' . $_SERVER['HTTP_HOST']);
                $this->email->message($this->load->view('email/contact', $this->input->post(), true));

                if ( ! $this->email->send()) {
                    // echo $this->email->print_debugger(); die;
                    log_message('error', $this->email->print_debugger());
                }

                $this->session->set_flashdata('success', 'Terima kasih telah mengirim pesan, Kami akan segera menghubungi kamu!');
                redirect('contact', 'refresh');
            }
        }

        $this->_render_template($this->web_dir . 'pages/contact/contact_form', $data);
    }
}

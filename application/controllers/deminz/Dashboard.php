<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('post_model');
        $this->load->driver('cache');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $data = [
            'page_title' => 'Dashboard'
        ];
        if ( ! $post_total = $this->cache->file->get('post_total')) {
            $post_total = Post_model::count();
            // 60 seconds = 1 menit
            $this->cache->file->save('post_total', $post_total, 60);
        }
        if ( ! $category_total = $this->cache->file->get('category_total')) {
            $category_total = Category_model::count();
            // 60 seconds = 1 menit
            $this->cache->file->save('category_total', $category_total, 60);
        }
        if ( ! $tag_total = $this->cache->file->get('tag_total')) {
            $tag_total = Tag_model::count();
            // 60 seconds = 1 menit
            $this->cache->file->save('tag_total', $tag_total, 60);
        }
        $data['post_total'] = $post_total;
        $data['category_total'] = $category_total;
        $data['tag_total'] = $tag_total;
        $page = "dashboard";
        $this->_render_template($this->admin_dir . 'pages/' . $page, $data);
    }
}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Editor extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    //Upload image summernote
    public function upload_image()
    {
        $this->load->library('upload');

        if (isset($_FILES["image"]["name"])) {
            $config['upload_path'] = './uploads/text-editor/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image')) {
                $this->upload->display_errors();
                return false;
            } else {
                $data = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/text-editor/' . $data['file_name'];
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = true;
                $config['quality'] = '80%';
                $config['width'] = 800;
                $config['height'] = 800;
                $config['new_image'] = './uploads/text-editor/' . $data['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo base_url() . 'uploads/text-editor/' . $data['file_name'];
            }
        }
    }

    //Delete image summernote
    public function delete_image()
    {
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if (unlink($file_name)) {
            echo 'File Delete Successfully';
        }
    }
}

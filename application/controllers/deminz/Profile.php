<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper(['form', 'url', 'inflector', 'language']);
        $this->load->library(['pagination', 'ion_auth', 'form_validation']);

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');

        $this->module_name = "Profile";
        // $this->group_options = Group_model::pluck('name', 'id');

        $this->global_data = $this->global_data + [
            'module_name' => $this->module_name,
            // 'group_options' => $this->group_options,
        ];

    }

    public function index()
    {
        $data = [
            'page_title' => 'My ' . $this->module_name,
            'user' => $this->current_user,
        ];

        $user = $this->current_user;
        foreach ((new User_model)->getFillable() as $field) {
            $data[$field] = $user->$field;
        }

        $this->_render_template($this->admin_dir . 'pages/profile/view', $data);
    }

    public function edit()
    {
        $data = [
            'page_title' => 'Edit ' . $this->module_name,
        ];

        $user = User_model::find($this->current_user->id);
        if (empty($user)) {
            show_error('Invalid ID!');die;
        }

        $data['id'] = $user->id;
        // $data['group_options'] = $this->group_options->prepend('---', '')->toArray();

        foreach ((new User_model)->getFillable() as $field) {
            $data[$field] = $user->$field;
        }

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'trim|required');
        $this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'trim|required');
        $this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'trim|required');
        $this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'trim|required');

        // update the password if it was posted
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
        }

        if ($this->form_validation->run()) {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
            );

            // update the password if it was posted
            if ($this->input->post('password')) {
                $data['password'] = $this->input->post('password');
            }

            // // Only allow updating groups if user is admin
            // if ($this->ion_auth->is_admin()) {
            //     // Update the groups user belongs to
            //     $groupData = $this->input->post('groups');

            //     if (isset($groupData) && !empty($groupData)) {

            //         $this->ion_auth->remove_from_group('', $id);

            //         foreach ($groupData as $grp) {
            //             $this->ion_auth->add_to_group($grp, $id);
            //         }

            //     }
            // }

            // // Save image profile if available
            // if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"] != "") {
            //     $image_path = 'uploads/users/';
            //     $image = $this->upload_image('image', $image_path);
            //     $attributes['image'] = $image;
            //     $attributes['image_path'] = $image_path;
            // }

            // check to see if we are updating the user
            if ($this->ion_auth->update($user->id, $data)) {
                // redirect them back to the admin page if admin, or to the base url if non admin
                $this->session->set_flashdata('success', $this->ion_auth->messages());
                $this->redirectIndex();

            } else {
                // redirect them back to the admin page if admin, or to the base url if non admin
                $this->session->set_flashdata('error', $this->ion_auth->errors());
                $this->redirectIndex();

            }
        }
        $this->_render_template($this->admin_dir . 'pages/profile/edit', $data);
    }

    /**
     * Redirect a user checking if is admin
     */
    public function redirectIndex()
    {
        if ($this->ion_auth->is_admin()) {
            redirect($this->admin_base_url . 'profile', 'refresh');
        }
        redirect($this->admin_base_url . 'dashboard', 'refresh');
    }
}

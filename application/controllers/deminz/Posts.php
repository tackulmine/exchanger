<?php defined('BASEPATH') or exit('No direct script access allowed');

class Posts extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper(['form', 'url', 'inflector']);
        $this->load->library(['pagination', 'form_validation']);
        $this->load->model(['post_model']);

        $this->module_name = "Post";
        $this->category_options = Category_model::pluck('title', 'id');
        $this->tag_options = Tag_model::pluck('title', 'title');

        $this->global_data = $this->global_data + [
            'module_name' => $this->module_name,
            'category_options' => $this->category_options,
            'tag_options' => $this->tag_options,
        ];
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->page();
    }

    public function page()
    {
        $data = [
            'page_title' => 'Manage ' . $this->module_name,
        ];

        $uri_segment = 4;
        $data['page'] = $page = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 0;
        $data['per_page'] = $per_page = 5;

        $post = new Post_model;

        if ($this->input->get('keyword')) {
            $post = $post->where('title', 'like', $this->input->get('keyword') . '%');
        }
        if ($this->input->get('filter_category')) {
            $post = $post->where('category_id', $this->input->get('filter_category'));
        }
        if ($this->input->get('filter_tag')) {
            $post = $post->whereHas('tags', function($q) {
                $q->where('id', $this->input->get('filter_tag'));
            });
        }
        if ($this->input->get('filter_tags')) {
            $post = $post->whereHas('tags', function($q) {
                $q->whereIn('id', $this->input->get('filter_tags'));
            });
        }
        if ($this->input->get('filter_draft')) {
            $post = $post->where('is_draft', $this->input->get('filter_draft') == 'n' ? 0 : 1);
        }

        $total_rows = $post->count();

        $data['posts'] = $post->with(['category', 'tags'])
            ->orderBy('id', 'desc')
            ->skip($page)
            ->take($per_page)
            ->get();

        $data['pagination'] = $this->set_pagination('posts/page', $total_rows, $per_page, $uri_segment, 2);

        $data['category_options'] = $this->category_options->prepend('category?', '')->toArray();
        // dd($data['category_options']);

        $this->_render_template($this->admin_dir . 'pages/post/list', $data);
    }

    public function create()
    {
        $data = [
            'page_title' => 'Create ' . $this->module_name,
        ];

        $data['tags'] = [];
        $data['category_options'] = $this->category_options->prepend('---', '')->toArray();
        $data['tag_options'] = $this->tag_options->toArray();

        foreach ((new Post_model)->getFillable() as $field) {
            $data[$field] = '';
        }

        $this->form_validation->set_rules('category_id', 'Category', 'trim|required|callback_category_check');
        $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[2]|max_length[255]');
        $this->form_validation->set_rules('subtitle', 'Subtitle', 'trim|required|min_length[2]|max_length[255]');
        $this->form_validation->set_rules('content_html', 'Description', 'trim|required|min_length[3]');
        $this->form_validation->set_error_delimiters('', '<br>');

        if ($this->form_validation->run()) {
            $attributes = $this->input->post();
            $attributes['slug'] = url_title($this->input->post('title'), '-', true);
            $attributes['is_draft'] = (bool) $this->input->post('is_draft');
            $attributes['created_by'] = $this->current_user->id;
            if ($this->input->post('published_at')) {
                $date = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $this->input->post('published_at') . ":00");
                $attributes['published_at'] = $date->format("Y-m-d H:i:s");
            }
            if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"] != "") {
                $image_path = 'uploads/posts/';
                $image = $this->upload_image('image', $image_path);
                $attributes['image'] = $image;
                $attributes['image_path'] = $image_path;
            }
            if (!$post = Post_model::create($attributes)) {
                show_error('Failed to create!');
            }
            // save the tags
            $post->syncTags($this->input->post('tags') ?? []);

            $this->session->set_flashdata('success', 'The item has been created!');
            redirect($this->admin_base_url . 'posts', 'refresh');
        }

        $this->_render_template($this->admin_dir . 'pages/post/create', $data);
    }

    public function edit(int $id)
    {
        $data = [
            'page_title' => 'Edit ' . $this->module_name,
        ];

        $post = Post_model::find($id);
        if (empty($post)) {
            show_error('Invalid ID!');die;
        }

        $data['id'] = $post->id;
        $data['tags'] = $post->tags()->pluck('title')->toArray();
        $data['category_options'] = $this->category_options->prepend('---', '')->toArray();
        $data['tag_options'] = $this->tag_options->toArray();

        foreach ((new Post_model)->getFillable() as $field) {
            $data[$field] = $post->$field;
        }

        $this->form_validation->set_rules('category_id', 'Category', 'trim|required|callback_category_check');
        $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[2]|max_length[255]');
        $this->form_validation->set_rules('subtitle', 'Subtitle', 'trim|required|min_length[2]|max_length[255]');
        $this->form_validation->set_rules('content_html', 'Description', 'trim|required|min_length[3]');

        if ($this->form_validation->run()) {
            $attributes = $this->input->post();
            // $attributes['slug'] = url_title($this->input->post('title'), '-', true);
            $attributes['is_draft'] = (bool) $this->input->post('is_draft');
            $attributes['updated_by'] = $this->current_user->id;
            if ($this->input->post('published_at')) {
                $date = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $this->input->post('published_at') . ":00");
                $attributes['published_at'] = $date->format("Y-m-d H:i:s");
            }
            if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"] != "") {
                $image_path = 'uploads/posts/';
                $image = $this->upload_image('image', $image_path);
                $attributes['image'] = $image;
                $attributes['image_path'] = $image_path;
            }
            if (!$post->update($attributes)) {
                show_error('Failed to update!');
            }
            // save the tags
            $post->syncTags($this->input->post('tags') ?? []);

            $this->session->set_flashdata('success', 'The item has been updated!');
            redirect($this->admin_base_url . 'posts', 'refresh');
        }
        $this->_render_template($this->admin_dir . 'pages/post/edit', $data);
    }

    public function category_check($category_id)
    {
        if (!Category_model::where('id', $category_id)->count()) {
            $this->form_validation->set_message('category_check', 'The {field} is not exists!');
            return false;
        } else {
            return true;
        }
    }

    public function delete(int $id)
    {
        $post = Post_model::find($id);
        if (empty($post)) {
            show_error('Invalid ID!');die;
        }

        if (!$post->delete()) {
            show_error('Failed to delete!');
        }
        $this->session->set_flashdata('success', 'The item has been deleted!');
        redirect($this->admin_base_url . 'posts', 'refresh');
    }
}

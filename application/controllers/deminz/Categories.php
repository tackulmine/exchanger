<?php defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper(['form', 'url', 'inflector']);
        $this->load->library(['pagination', 'form_validation']);
        $this->load->model(['category_model']);

        $this->module_name = "Category";

        $this->global_data = $this->global_data + [
            'module_name' => $this->module_name,
        ];
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->page();
    }

    public function page()
    {
        $data = [
            'page_title' => 'Manage ' . $this->module_name,
        ];

        $uri_segment = 4;
        $data['page'] = $page = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 0;
        $data['per_page'] = $per_page = 5;

        $category = new Category_model;

        if ($this->input->get('keyword')) {
            $category = $category->where('title', 'like', $this->input->get('keyword') . '%');
        }

        $total_rows = $category->count();

        $data['categories'] = $category->withCount(['posts'])
            ->orderBy('title', 'asc')
            ->skip($page)
            ->take($per_page)
            ->get();

        $data['pagination'] = $this->set_pagination('categories/page', $total_rows, $per_page, $uri_segment, 2);

        $this->_render_template($this->admin_dir . 'pages/category/list', $data);
    }

    public function create()
    {
        $data = [
            'page_title' => 'Create ' . $this->module_name,
        ];

        foreach ((new Category_model)->getFillable() as $field) {
            $data[$field] = '';
        }

        $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[2]|regex_match[/^[A-Za-z ]+$/]|max_length[255]|is_unique[categories.title]',
            [
                'regex_match' => 'Silakan input dengan hanya alfabet dan spasi.',
                'is_unique' => 'Gunakan {field} yang lain.',
            ]
        );
        $this->form_validation->set_error_delimiters('', '<br>');

        if ($this->form_validation->run()) {
            $attributes = $this->input->post();
            $attributes['slug'] = url_title($this->input->post('title'), '-', true);
            $attributes['is_draft'] = (bool) $this->input->post('is_draft');
            $attributes['created_by'] = $this->current_user->id;
            // var_dump($attributes); die;
            if (!$tag = Category_model::create($attributes)) {
                show_error('Failed to create!');die;
            }

            $this->session->set_flashdata('success', 'The item has been created!');
            redirect($this->admin_base_url . 'categories', 'refresh');
        }

        $this->_render_template($this->admin_dir . 'pages/category/create', $data);
    }

    public function edit(int $id)
    {
        $data = [
            'page_title' => 'Edit ' . $this->module_name,
        ];

        $tag = Category_model::find($id);
        if (empty($tag)) {
            show_error('Invalid ID!');die;
        }

        $data['id'] = $tag->id;

        foreach ((new Category_model)->getFillable() as $field) {
            $data[$field] = $tag->$field;
        }

        $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[2]|max_length[255]|regex_match[/^[A-Za-z ]+$/]|callback_title_check[' . $id . ']',
            ['regex_match' => 'Silakan input dengan hanya alfabet dan spasi.']
        );
        $this->form_validation->set_error_delimiters('', '<br>');

        if ($this->form_validation->run()) {
            $attributes = $this->input->post();
            // $attributes['slug'] = url_title($this->input->post('title'), '-', true);
            $attributes['is_draft'] = (bool) $this->input->post('is_draft');
            $attributes['updated_by'] = $this->current_user->id;
            // var_dump($attributes); die;
            if (!$tag->update($attributes)) {
                show_error('Failed to update!');die;
            }

            $this->session->set_flashdata('success', 'The item has been updated!');
            redirect($this->admin_base_url . 'categories', 'refresh');
        }
        $this->_render_template($this->admin_dir . 'pages/category/edit', $data);
    }

    public function title_check(string $str, int $id)
    {
        if (Category_model::where('title', $str)->where('id', '<>', $id)->count()) {
            $this->form_validation->set_message('title_check', 'Gunakan {field} yang lain!');
            return false;
        } else {
            return true;
        }
    }

    public function delete(int $id)
    {
        $tag = Category_model::find($id);
        if (empty($tag)) {
            show_error('Invalid ID!');die;
        }

        if (!$tag->delete()) {
            show_error('Failed to delete!');die;
        }
        $this->session->set_flashdata('success', 'The item has been deleted!');
        redirect($this->admin_base_url . 'categories', 'refresh');
    }
}

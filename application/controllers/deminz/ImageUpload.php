<?php defined('BASEPATH') or exit('No direct script access allowed');

class ImageUpload extends MY_Controller
{

    /**
     * Manage __construct
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'url']);
    }

    /**
     * Manage index
     *
     * @return Response
     */
    public function index()
    {
        $this->load->view($this->admin_dir . 'imageUploadForm', ['error' => '']);
    }

    /**
     * Manage uploadImage
     *
     * @return Response
     */
    public function uploadImage()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $this->load->library('upload', $config);

        if (!is_dir(FCPATH . '/uploads')) {
            mkdir(FCPATH . '/uploads', 0777, true);
        }

        if (!$this->upload->do_upload('image')) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view($this->admin_dir . 'imageUploadForm', $error);
        } else {
            $uploadedImage = $this->upload->data();
            $this->resizeImage($uploadedImage['file_name']);

            print_r('Image Uploaded Successfully.');
            exit;
        }
    }

    /**
     * Manage uploadImage
     *
     * @return Response
     */
    public function resizeImage($filename, $dir = 'uploads/')
    {
        $dir = rtrim($dir, '/');
        $source_path = $_SERVER['DOCUMENT_ROOT'] . '/' .  $dir . '/' . $filename;
        $target_path = $_SERVER['DOCUMENT_ROOT'] . '/' .  $dir . '/' . thumbnail . '/';

        if (!is_dir(FCPATH . $target_path)) {
            mkdir(FCPATH . $target_path, 0777, true);
        }

        $config_manip = [
            'image_library' => 'gd2',
            'source_image' => FCPATH . $source_path,
            'new_image' => FCPATH . $target_path,
            'maintain_ratio' => true,
            'create_thumb' => true,
            'thumb_marker' => '_thumb',
            'width' => 150,
            'height' => 150,
        ];

        $this->load->library('image_lib', $config_manip);

        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }

        $this->image_lib->clear();
    }
}

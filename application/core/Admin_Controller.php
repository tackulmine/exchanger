<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin_Controller extends MY_Controller
{
    protected $admin_dir = null;
    protected $admin_path = null;
    protected $admin_base_url = null;
    protected $current_user = null;
    protected $module_name = null;

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library(['ion_auth']);
        $this->load->model(['user_model']);
        $this->load->helper(['gravatar']);

        $this->template_dir = 'templates/backend/';
        $this->admin_dir = 'backend' . DIRECTORY_SEPARATOR;
        $this->admin_path = APPPATH . 'views' . DIRECTORY_SEPARATOR . 'backend' . DIRECTORY_SEPARATOR;

        $this->admin_base_url = 'deminz/';

        $this->global_data = $this->global_data + [
            'admin_dir' => $this->admin_dir,
            'admin_base_url' => $this->admin_base_url,
        ];

        $this->_check_logged_in_user();
    }

    private function _check_logged_in_user()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect($this->admin_base_url . 'auth/login', 'refresh');
            exit;
        } else if (!$this->ion_auth->is_admin()) {
            // remove this elseif if you want to enable this for non-admins
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }

        $current_user = $this->ion_auth->user()->row();
        $this->current_user = User_model::find($current_user->id);

        $this->global_data = $this->global_data + [
            'current_user' => $this->current_user,
        ];
    }

    protected function set_pagination(string $url, int $total_rows, int $per_page = 5, int $uri_segment = 4, int $num_links = null)
    {
        $this->load->library(['pagination']);
        //konfigurasi pagination
        $config['base_url'] = site_url($this->admin_base_url . $url); //site url
        $config['total_rows'] = $total_rows; //total row
        $config['per_page'] = $per_page; //show record per halaman
        $config["uri_segment"] = $uri_segment; // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = !empty($num_links) ? $num_links : floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center m-0">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close'] = '</span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close'] = '</span></li>';

        if ($this->input->get()) {
            $get_params = array_filter($this->input->get());
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($get_params);
            $config['suffix'] = '?' . http_build_query($get_params);
        }
        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }

    protected function upload_image($file_name, $image_path = 'uploads/')
    {
        if (!is_dir(FCPATH . $image_path)) {
            mkdir(FCPATH . $image_path, 0777, true);
        }
        $this->load->library('upload');
        $config['upload_path'] = './' . $image_path;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $this->upload->initialize($config);
        if ($this->upload->do_upload($file_name)) {
            $data = $this->upload->data();
            //Compress Image
            $config['image_library'] = 'gd2';
            $config['source_image'] = './' . $image_path . $data['file_name'];
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = true;
            $config['quality'] = '80%';
            $config['width'] = 800;
            $config['height'] = 800;
            $config['new_image'] = './' . $image_path . $data['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            // create thumbnail
            // resizeImage($data['file_name'], $image_path);
        }
        return $data['file_name'];
    }
}

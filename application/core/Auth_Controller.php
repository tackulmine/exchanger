<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth_Controller extends MY_Controller
{
    protected $web_dir = null;
    protected $web_path = null;
    protected $web_base_url = null;

    public function __construct()
    {
        parent::__construct();

        $this->template_dir = 'templates/auth/';
        $this->web_dir = 'auth/';
        $this->web_path = APPPATH . 'views' . DIRECTORY_SEPARATOR . 'auth' . DIRECTORY_SEPARATOR;

        $this->web_base_url = 'auth/';

        $this->global_data = $this->global_data + [
            'web_dir' => $this->web_dir,
            'web_base_url' => $this->web_base_url,
        ];

        if ($this->config->item('site_open') === false) {
            show_error('Sorry the site is shut for now.');
        }
    }
}

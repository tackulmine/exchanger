<?php defined('BASEPATH') or exit('No direct script access allowed');

@include_once(APPPATH . 'core/Auth_Controller.php');
@include_once(APPPATH . 'core/Admin_Controller.php');
@include_once(APPPATH . 'core/Public_Controller.php');

class MY_Controller extends CI_Controller
{
    protected $global_data = [];
    protected $template_dir = 'templates/frontend/';

    public function __construct()
    {
        parent::__construct();

        if (ENVIRONMENT === 'development') {
            if (!$this->input->is_ajax_request()) {
                $this->output->enable_profiler(TRUE);
            }
        }
    }

    /**
     * @param string     $view
     * @param array|null $data
     * @param bool       $return_html
     *
     * @return mixed
     */
    protected function _render_page($view, $data = [], $return_html = false) //I think this makes more sense
    {
        if ( ! is_file(APPPATH . "views" . DIRECTORY_SEPARATOR . $view . '.php')) {
            if ($return_html) {
                die("404");
            }
            // Whoops, we don't have a page for that!
            show_404();
        }

        $this->global_data = array_merge($this->global_data, $data);

        $view_html = $this->load->view($view, $this->global_data, $return_html);

        // This will return html on 3rd argument being true
        if ($return_html) {
            return $view_html;
        }
    }

    /**
     * @param string     $view
     * @param array|null $data
     *
     * @return mixed
     */
    protected function _render_template($view, $data = []) //I think this makes more sense
    {
        $this->global_data = array_merge($this->global_data, $data);

        $this->load->view($this->template_dir . 'header', $this->global_data);
        $this->_render_page($view, $data);
        $this->load->view($this->template_dir . 'footer', $this->global_data);
    }
}
<?php defined('BASEPATH') or exit('No direct script access allowed');

class Public_Controller extends MY_Controller
{
    protected $web_dir = null;
    protected $web_path = null;
    protected $web_base_url = null;

    public function __construct()
    {
        parent::__construct();

        $this->template_dir = 'templates/frontend/';
        $this->web_dir = 'frontend/';
        $this->web_path = APPPATH . 'views' . DIRECTORY_SEPARATOR . 'frontend' . DIRECTORY_SEPARATOR;

        $this->web_base_url = '';

        $this->global_data = $this->global_data + [
            'web_dir' => $this->web_dir,
            'web_base_url' => $this->web_base_url,
        ];

        if ($this->config->item('site_open') === false) {
            show_error('Sorry the site is shut for now.');
        }

        // If the user is using a mobile, use a mobile theme
        // $this->load->library('user_agent');
        // if ($this->agent->is_mobile()) {
            /*
             * Use my template library to set a theme for your staff
             *     http://philsturgeon.co.uk/code/codeigniter-template
             */
        //     $this->template->set_theme('mobile');
        // }
    }

    protected function set_pagination(string $url, int $total_rows, int $per_page = 5, int $uri_segment = 3, int $num_links = null)
    {
        $this->load->library(['pagination']);
        //konfigurasi pagination
        $config['base_url'] = site_url($this->web_base_url . $url); //site url
        $config['total_rows'] = $total_rows; //total row
        $config['per_page'] = $per_page; //show record per halaman
        $config["uri_segment"] = $uri_segment; // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = !empty($num_links) ? $num_links : floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = '<i class="fas fa-angle-double-left"></i><span class="sr-only">Pertama</span>';
        $config['last_link'] = '<i class="fas fa-angle-double-right"></i><span class="sr-only">Terakhir</span>';
        $config['next_link'] = '<i class="fas fa-angle-right"></i><span class="sr-only">Selanjutnya</span>';
        $config['prev_link'] = '<i class="fas fa-angle-left"></i><span class="sr-only">Sebelumnya</span>';
        $config['full_tag_open'] = '<nav><ul class="pagination pagination-template d-flex justify-content-center">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item"><span class="page-link active">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close'] = '</span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close'] = '</span></li>';

        if ($this->input->get()) {
            $get_params = array_filter($this->input->get());
            $config['first_url'] = $config['base_url'] . '?' . http_build_query($get_params);
            $config['suffix'] = '?' . http_build_query($get_params);
        }
        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }
}
